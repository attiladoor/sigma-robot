import sys
import os
import numpy as np
sys.path.append(os.path.join(os.path.dirname(__file__), '../misc'))
from params import *

def getLineCoordinates(rho, theta):
    """
    Calculates start and end point of Hough-lines for displaying purposes
    Args:
        rho: Hough-line rho (pixels)
        theta: Hough-line theta (radian)
    Returns:
        start point and end point coordinates [[x1, y1], [x2, y2]]
    """
    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a*rho
    y0 = b*rho
    x1 = int(x0 + 1000*(-b))
    y1 = int(y0 + 1000*(a))
    x2 = int(x0 - 1000*(-b))
    y2 = int(y0 - 1000*(a))
    return [[x1, y1], [x2 ,y2]]

def getLineIntersection(rho1, theta1, rho2, theta2, width=1280, height=720):
    """
    Get intersection coordinates of two lines
    Args:
        rho1: rho parameret of the 1st Hough-line
        theta1: theta parameter of the 1st Hough-line
        rho2: rho parameret of the 2nd Hough-line
        theta2: theta parameter of the 2nd Hough-line
    Returns:
        intersection coordinates (x,y)
    Raises:
        ValueError if intersection is off the image or there is no intersection
    """
    coords1 = getLineCoordinates(rho1, theta1)
    coords2 = getLineCoordinates(rho2, theta2)

    xdiff = (coords1[0][x_plot_i] - coords1[1][x_plot_i], coords2[0][x_plot_i] - coords2[1][x_plot_i])
    ydiff = (coords1[0][y_plot_i] - coords1[1][y_plot_i], coords2[0][y_plot_i] - coords2[1][y_plot_i]) 

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
        raise ValueError('lines do not intersect')

    d = (det(*coords1), det(*coords2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div

    if x < 0 or y < 0 or x > width  or y > height:
        raise ValueError('lines do not intersect')

    return np.array([int(x), int(y)])