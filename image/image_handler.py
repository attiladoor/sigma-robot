import threading
import cv2
import sys
import os
import numpy as np
import time
sys.path.append(os.path.join(os.path.dirname(__file__), '../misc'))

from logger import Logger
from image_processor import ImageProcessor
from params import *

CALIB_PATH = os.path.join(os.path.dirname(__file__), '../calibration/')

class ImageHandler (threading.Thread):
    """
    Helper class for processing camera image.

    It delegates the camera image handling into an independent thread an provides game status data after processing.
    """

    def click_point(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            self.points.append((x, y))
        
    def calibrate_points(self, cap):
        print("Click to insert the 4 corners of the grid in the image, then press q.")
        while(cap.isOpened()):
            ret, frame = cap.read()   
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            for point in self.points:
                cv2.circle(gray, point, 3, (255, 0, 0), -1)
            cv2.namedWindow("calib_image")
            cv2.setMouseCallback("calib_image", self.click_point)
            cv2.imshow("calib_image", gray)
            time.sleep(0.1)  
            if cv2.waitKey(1) & (0xFF == ord('q') or len(self.points) == 4):
                cv2.destroyAllWindows()
                break 

    def warp_image(self, frame):
        rect = np.float32(self.points)
        (tl, tr, br, bl) = rect

        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))

        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))

        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype = "float32")

        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(frame, M, (maxWidth, maxHeight))
        return warped, maxHeight, maxWidth

    def __init__(self, isDebug, cameraID):
        """
        Constructor
        Args:
            isDebug: boolean if debug printout is activated
            cameraID: integer designating the camera port to be used
        """
        threading.Thread.__init__(self)
        self.logger = Logger(isDebug, "imageHandler")
        self.threadLock = threading.Lock()
        self.runFlag = True
        self.points = []
        self.stableStatus = np.full((3,3), N_i)
        self.gameStatus = self.stableStatus
        self.stableCounters = np.zeros((3, 3), 'int')

        self.cameraCapture = cv2.VideoCapture(cameraID + cv2.CAP_DSHOW)
        self.cameraCapture.set(cv2.CAP_PROP_BUFFERSIZE, 1)
        if not self.cameraCapture.isOpened():
            self.logger.fail("unable to open camera")
            raise SystemError("unable to open camera")

        #camera calibration parameters
        self.cameraMtx = np.loadtxt(CALIB_PATH + "calib_matrix.txt")
        self.cameraDist = np.loadtxt(CALIB_PATH + "calib_dist.txt")

        self.cameraCapture.set(cv2.CAP_PROP_FRAME_WIDTH, CAMERA_W)
        self.cameraCapture.set(cv2.CAP_PROP_FRAME_HEIGHT, CAMERA_H)

        w = int(self.cameraCapture.get(cv2.CAP_PROP_FRAME_WIDTH))
        h = int(self.cameraCapture.get(cv2.CAP_PROP_FRAME_HEIGHT))

        self.cameraMtx, self.roi = cv2.getOptimalNewCameraMatrix(self.cameraMtx, self.cameraDist, (w,h), 1, (w,h))

        # Perform initial warp-calibration
        self.calibrate_points(self.cameraCapture)

        # Open first image to get warp-dimensions
        ret, frame = self.cameraCapture.read()
        first_warped, h, w = self.warp_image(frame)

        # Create imageprocessor using warped dim
        self.imageProcessor = ImageProcessor(w, h, ifPlot=True, isDebug=isDebug)

        self.logger.system("Camera initialized")

    def run(self):
        """
        Main image processing function

        It executes the following operations:
        - reading camera image
        - undistorting according to calibration matrix
        - utlizing "ImageProcessor" for extracting game status data + marking game symbols on image
        - updating game status data
        """
        
        while self.runFlag:
            ret, frame = self.cameraCapture.read()
            # undistort
            dst = cv2.undistort(frame, self.cameraMtx, self.cameraDist, None, self.cameraMtx)
            # warp the image
            dst, h, w = self.warp_image(frame)
            try:
                img, status = self.imageProcessor.updateImage(dst)
                self.threadLock.acquire()
                self.prevStatus = self.gameStatus
                self.gameStatus = status
                self.setStableStatus()
                self.threadLock.release()
                cv2.imshow('frame', img)
                self.logger.debug(self.getStatus())
            except Exception as e:
                self.logger.debug(str(e))
                #cv2.imshow('frame',dst)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    
    def stop(self):
        """
        Stops thread execution
        """
        self.runFlag = False

    def getStatus(self):
        """
        Getter function for game status
        Returns:
            3 x 3 numpy matrix with game status information
        """
        self.threadLock.acquire()
        status = self.gameStatus
        self.threadLock.release()
        return status

    def setStableStatus(self):
        """
        Setter function for stable game status
        """
        status = self.gameStatus
        for y in range(status.shape[1]):
            for x in range(status.shape[0]):
                if status[x][y] == self.prevStatus[x][y]:
                    self.stableCounters[x][y] = self.stableCounters[x][y] + 1
                    if self.stableCounters[x][y] >= stable_samples:
                        self.stableStatus[x][y] = status[x][y]
                else:
                    self.stableCounters[x][y] = 0

    def getStableStatus(self):
        """
        Getter function for stable game status
        Returns:
            Game state that has been stable over the specified amount of samples
        """
        return self.stableStatus                