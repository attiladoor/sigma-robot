from __future__ import absolute_import, division, print_function

# Helper libraries
import numpy as np
import os
import sys
# TensorFlow and tf.keras
import tensorflow as tf
import cv2
from PIL import Image
from os import listdir
from tensorflow import keras
from tensorflow.python.keras.backend import set_session
MODEL_PATH=os.path.join(os.path.dirname(__file__), 'model/model.hdf5')
sys.path.append(os.path.join(os.path.dirname(__file__), '../misc'))
from params import *

class ClassifierClass:
    # This AI-class does not have a function for training since that is already taken care of by the training-code in the "classification" folder.
    # This class simply loads the weights trained by that classifier (meaning the model definitions needs to be the same) and makes predictions
    # The reason why the same class was not re-used was because the one in the "classification" was created with batch-training in mind, and it was tedious to run single images through, if I remember correctly.
    def __init__(self):
        # Create Keras model
        self.model = keras.Sequential()
        self.model.add(keras.layers.Conv2D(32, kernel_size=(5, 5), strides=(1, 1),
                                      activation=tf.nn.relu, input_shape=(28, 28, 1)))
        self.model.add(keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
        self.model.add(keras.layers.Conv2D(64, (5, 5), activation=tf.nn.relu))
        self.model.add(keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
        self.model.add(keras.layers.Flatten())
        self.model.add(keras.layers.Dense(1024, activation=tf.nn.relu))
        self.model.add(keras.layers.Dense(3, activation=tf.nn.softmax))
        print('Model created!')

        self.model.compile(optimizer='adam',
                      loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])
        self.graph = tf.compat.v1.get_default_graph()
        self.sess = tf.compat.v1.Session()
        tf.compat.v1.enable_eager_execution()
        if os.path.isfile(MODEL_PATH):
            set_session(self.sess)
            # Load weights from Model_path, session needs to be set for whichever reason I don't remember, but removing it will fail the code unless this has been fixed in newer versions of Keras
            self.model.load_weights(MODEL_PATH)
            self.model._make_predict_function()


    def predict(self, img):
        img_resized = cv2.resize(img, (28, 28), interpolation=cv2.INTER_LINEAR)      
        img_reshaped_model = tf.reshape(img_resized.astype(np.float64), [1, 28, 28, 1])
        #Data should be on format [1,28,28,1]
        cv2.waitKey(1)
        # Return classification prediction (EMPTY/X/O) based on normalizer image (hence divide by 255)
        return self.model.predict(img_reshaped_model / 255, steps=1)

    def loadweights(self):
        self.model.load_weights(MODEL_PATH)