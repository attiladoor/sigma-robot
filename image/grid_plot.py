import sys
import os
import numpy as np
import image_utils
sys.path.append(os.path.join(os.path.dirname(__file__), '../misc'))
from params import *

class GridPlot:
    """
    Helper class for ImageProcessor
    It calculates tic-tac-toe grid parameters
    """
    def __init__(self, vertical_lines, horizontal_lines, width, height):

        self.width = width
        self.height = height
        self.grid_dots = np.zeros((len(vertical_lines), len(horizontal_lines), 2))

        if self.grid_dots.shape != (grid_lines_num, grid_lines_num, 2):
            raise ValueError("input dimensions failure: ", self.grid_dots.shape)

        #filling up dots
        for vl in range(len(vertical_lines)):
            for hl  in range(len(horizontal_lines)):
                self.grid_dots[vl][hl] = \
                image_utils.getLineIntersection(vertical_lines[vl][rho_plot_i], vertical_lines[vl][theta_plot_i], horizontal_lines[hl][rho_plot_i], horizontal_lines[hl][theta_plot_i], width, height)

    def getGridLineCoords(self):
        """
        Composes a matrix of grid lines. 
        It extract coordinates from "grid dots" that cover up all the lines in the grid. Practically these are coordinate 
        pairs of the dots on the edge of the grid. 
        
        Returns:
            grid lines matrix. It has the shape of (8,2,2). 8 lines, 2 coordinates/line, 2 values/coordinate
        """
        dim = self.grid_dots.shape[0]
        grid_lines = np.zeros((2 * dim, 2, 2))
        
        # horizontal lines
        for i in range(dim):
            grid_lines[i][x_plot_i] =  self.grid_dots[i][x_plot_i]
            grid_lines[i][y_plot_i] =  self.grid_dots[i][dim - y_plot_i]

        # vertical lines
        for i in range(dim):
            grid_lines[dim + i][x_plot_i] =  self.grid_dots[x_plot_i][i]
            grid_lines[dim + i][y_plot_i] =  self.grid_dots[dim -y_plot_i][i]
        
        return grid_lines.astype(int)

    def getGridFieldCoordinates(self, x, y):
        """
        Get square coordinates of the selected grid field square
        It selects the biggest square that fits to the field
        Args:
            x: x axis position of the selected field (0,2)
            y: y axis position of the selected field (0,2)
        Returns:
            top_left_final_c: coordinate pair of the top left corner
            bottom_right_final_c: coordinate pair of bottom left cordner
        """
        if x > 3 or y > 3:
            raise ValueError("incorrect arguments")

        top_left_final_c = np.zeros((2))
        bottom_right_final_c = np.zeros((2))
      
        ### top left x
        if self.grid_dots[x][y][x_plot_i] > self.grid_dots[x][y + 1][x_plot_i]:
            top_left_final_c[x_plot_i] = self.grid_dots[x][y][x_plot_i] + grid_field_frame_px
        else:
            top_left_final_c[x_plot_i] = self.grid_dots[x][y + 1][x_plot_i] + grid_field_frame_px

        ### top left y
        if self.grid_dots[x][y][y_plot_i] > self.grid_dots[x + 1][y][y_plot_i]:
            top_left_final_c[y_plot_i] = self.grid_dots[x][y][y_plot_i] + grid_field_frame_px
        else:
            top_left_final_c[y_plot_i] = self.grid_dots[x + 1][y][y_plot_i] + grid_field_frame_px

        ### bottom right x
        if self.grid_dots[x + 1][y + 1][x_plot_i] < self.grid_dots[x + 1][y][x_plot_i]:
            bottom_right_final_c[x_plot_i] = self.grid_dots[x + 1][y + 1][x_plot_i] - grid_field_frame_px
        else:
            bottom_right_final_c[x_plot_i] = self.grid_dots[x + 1][y][x_plot_i] - grid_field_frame_px

        ### bottom right y
        if self.grid_dots[x + 1][y + 1][y_plot_i] < self.grid_dots[x][y + 1][y_plot_i]:
            bottom_right_final_c[y_plot_i] = self.grid_dots[x + 1][y + 1][y_plot_i] - grid_field_frame_px
        else:
            bottom_right_final_c[y_plot_i] = self.grid_dots[x][y + 1][y_plot_i] - grid_field_frame_px

        return top_left_final_c.astype(int), bottom_right_final_c.astype(int)

    def getGridDotsCoords(self):
        """
        Get function of all the dot coordinates at the intersections of grid lines
        Returns:
            4 x 4 x 2 matrix of the grid dots
        """
        return self.grid_dots.reshape((grid_lines_num * grid_lines_num, 2)).astype(int)
