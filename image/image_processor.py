import os
import sys
import numpy as np
sys.path.append(os.path.join(os.path.dirname(__file__), '../misc'))
from logger import Logger
import cv2
import grid_plot
from grid_plot import GridPlot
import image_utils
from ClassifierClass import ClassifierClass
from enum import Enum
from params import *

class Symbols(Enum):
    X = 0
    O = 1
    EMPTY = 2

class LineType(Enum):
    VERTICAL = 0
    HORIZONTAL = 1

class ImageProcessor:
    """
    Class for processing tic-tac-toe game field image
    1) It detects the lines on the game field
    2) Removes false positives and selects only the ones from the grid
    3) Extracts symbol squares and detects their value
    """
    def __init__(self, img_w, img_h, ifPlot=False, isDebug=False):
        
        self.ifPlot = ifPlot
        self.isDebug = isDebug 
        self.logger = Logger(isDebug, "imageProcessor")
        self.img_w = img_w
        self.img_h = img_h
        self.classifierClass = ClassifierClass()

    def __colorSquare(self, top_left_c, bottom_right_c, color, img):
        """
        Adds a colorful square on an image between the given corners
        Args:
            top_left_c: top left corner coordinates of the square (pixel pair)
            bottom_right_c: bottom right corner coordinates of the square (pixel pair)
            color: RGB color code of square 
            img: input image
        Returns:
            Updated image
        """
        dx = bottom_right_c[x_plot_i] - top_left_c[x_plot_i]
        dy = bottom_right_c[y_plot_i] - top_left_c[y_plot_i]
        def update_pixel(original, addition):
            original[0] = original[0] + addition[0] if (original[0] + addition[0]) <= 255 else 255
            original[1] = original[1] + addition[1] if (original[1] + addition[1]) <= 255 else 255
            original[2] = original[2] + addition[2] if (original[2] + addition[2]) <= 255 else 255
            return original

        for yi in range(dy):
            for xi in range(dx):
                img[top_left_c[y_plot_i] + yi][top_left_c[x_plot_i] + xi]= update_pixel(img[top_left_c[y_plot_i] + yi][top_left_c[x_plot_i] + xi], color)

        return img     


    def __drawLinesInFrame(self, lines, frame, RGB):
        for line in lines:
            rho = line[0]
            theta = line[1]
            a = np.cos(theta)
            b = np.sin(theta)
            x0 = a*rho
            y0 = b*rho
            x1 = int(x0 + 1000*(-b))
            y1 = int(y0 + 1000*(a))
            x2 = int(x0 - 1000*(-b))
            y2 = int(y0 - 1000*(a))

            cv2.line(frame, (x1,y1), (x2,y2), RGB, 2)
        cv2.imshow('DetectedLines', frame)
        cv2.waitKey(1)


    def __getLineMidIntersection(self, lines, lineType):
        """
        Calculates the intersection of all the lines with an imaginary mid-baseline
        Args:
            lines: matrix of Hough-lines [[rho1, theta1], [rho2, theta2]...]
            lineType: LineType enum
        Returns:
            Updated matrix where [[rho1, theta1, x1, y1], [rho2, theta2, x2, y2]...]
        Raises:
            ValueError if unknown LineType is selected
        """
        
        base_line = np.zeros((2))
        if lineType == LineType.VERTICAL:
            base_line[rho_plot_i] = self.img_h / 2
            base_line[theta_plot_i] = np.pi / 2 # 90 degrees

        elif lineType == LineType.HORIZONTAL:
            base_line[rho_plot_i] = self.img_w / 2
            base_line[theta_plot_i] = 0 # 0 degrees
        else:
            raise ValueError('wrong line type')

        i = 0
        for l in lines:
            intersection_coordinates = image_utils.getLineIntersection(base_line[rho_plot_i], base_line[theta_plot_i], \
                l[rho_plot_i], l[theta_plot_i], self.img_w, self.img_h).astype(int)
            lines[i] = np.append(lines[i] ,intersection_coordinates.astype(int))
            i += 1
            
        return np.array(lines)


    def __sortingLines(self, lines, lineType):
        """
        Sorting the lines according to their intersection with the mid-baseline
        Args:
            lines: line matrix where [[rho1, theta1, x1, y1], [rho2, theta2, x2, y2]...]
            lineType: LineType enum
        Returns:
            Updated lines where Verticals are sorted by X and Horizontals by Y
        Raises:
            ValueError if unknown LineType is selected
        """
        for i1 in range(lines.shape[0]):
            tmp_i = i1
            for i2 in range(i1 + 1, lines.shape[0]):

                if lineType == lineType.VERTICAL:
                     if lines[i2][x_idx] < lines[tmp_i][x_idx]:
                         tmp_i = i2
                elif lineType == lineType.HORIZONTAL:
                     if lines[i2][y_idx] < lines[tmp_i][y_idx]:
                         tmp_i = i2
                else:
                    raise ValueError('wrong line type')
            
            if not tmp_i == i1:
                tmp = np.array(lines[i1])
                lines[i1] = lines[tmp_i]
                lines[tmp_i] = tmp

        return lines


    def __mergeLinesByDistance(self, lines_w_intersect, lineType):
        """
        Merges neighbour lines if they are close to each other
        Args:
            lines_w_intersect: line matrix where [[rho1, theta1, x1, y1], [rho2, theta2, x2, y2]...]
            lineType: LineType enum
        Returns:
            Updated matrix where neigbour lines are merged
        Raises:
            ValueError if unknown LineType is selected
        """
        WEIGHT_INDEX = 4

        matrix = np.ones((lines_w_intersect.shape[0],lines_w_intersect.shape[1] + 1))
        matrix[:,:-1] = lines_w_intersect
       
        def addWithWeight(value1, weight1, value2, weight2):
            return (value1 * weight1 + value2 * weight2) / (weight1 + weight2)

        for i1 in range(matrix.shape[0]):
            for i2 in range(i1 + 1, matrix.shape[0]):

                merge = False
                if lineType == LineType.VERTICAL:
                    if abs(matrix[i1][x_idx] - matrix[i2][x_idx]) < self.img_w * distance_e:
                        merge = True
                elif lineType == LineType.HORIZONTAL:    
                    if abs(matrix[i1][y_idx] - matrix[i2][y_idx]) < self.img_h * distance_e:
                        merge = True

                if merge:
                    matrix[i2][rho_plot_i] = addWithWeight(matrix[i1][rho_plot_i], matrix[i1][WEIGHT_INDEX], matrix[i2][rho_plot_i], matrix[i2][WEIGHT_INDEX])
                    matrix[i2][theta_plot_i] = addWithWeight(matrix[i1][theta_plot_i], matrix[i1][WEIGHT_INDEX], matrix[i2][theta_plot_i], matrix[i2][WEIGHT_INDEX])
                    matrix[i2][x_idx] = addWithWeight(matrix[i1][x_idx], matrix[i1][WEIGHT_INDEX], matrix[i2][x_idx], matrix[i2][WEIGHT_INDEX])
                    matrix[i2][y_idx] = addWithWeight(matrix[i1][y_idx], matrix[i1][WEIGHT_INDEX], matrix[i2][y_idx], matrix[i2][WEIGHT_INDEX])
                    matrix[i2][WEIGHT_INDEX] = matrix[i1][WEIGHT_INDEX] + matrix[i2][WEIGHT_INDEX]
                    matrix[i1][WEIGHT_INDEX] = 0
                    break

        merged_lines = []
        for l in matrix:
            if l[WEIGHT_INDEX]:
                merged_lines.append(l[rho_plot_i:WEIGHT_INDEX])
        
        return np.array(merged_lines)


    def __getMostSignificantDistance(self, lines, lineType):
        """
        Calculates the distance (average) of lines that is included the most in the array
        Args:
            lines: multi dimensional sorted Hough line array, where 2.dim[0] = rho, 2.dim[1] = theta param
        Returns:
            distance (integer)
        """
        #calculating distances
        distances = []
        for l in range(len(lines) - 1): 

            if lineType == LineType.VERTICAL:
                distance = lines[l + 1][x_idx] - lines[l][x_idx]
            elif lineType == LineType.HORIZONTAL:    
                distance = lines[l + 1][y_idx] - lines[l][y_idx]
            else:
                raise ValueError('wrong line type')
            
            distances.append(distance)
        
        #merging and weighting distances
        weights = np.ones(len(distances))
        for v1 in range(len(distances)):
            for v2 in range(v1 + 1, len(distances)):
                if abs(distances[v1] - distances[v2]) < distance_e * distances[v1]:
                    distances[v2] = (weights[v1] * distances[v1] + weights[v2] * distances[v2]) / (weights[v1] + weights[v2])
                    weights[v2] += weights[v1]
                    weights[v1] = 0

        return distances[np.argmax(weights)]


    def __filteringLinesByDistance(self, lines_w_intersect, lineType):
        """
        Filtering out lines with unexpected distance from others
        Args:
            lines_w_intersect: line matrix where [[rho1, theta1, x1, y1], [rho2, theta2, x2, y2]...]
            lineType: LineType enum        
        Return:
            updated line matrix 
        Raises:
            ValueError if unknown LineType is selected       
        """
        MSDistance = self.__getMostSignificantDistance(lines_w_intersect, lineType)
        lines_filtered = []
        for l1 in range(lines_w_intersect.shape[0]):
            for l2 in range(l1 + 1, lines_w_intersect.shape[0]):
                
                if lineType == LineType.VERTICAL:
                    distance = lines_w_intersect[l2][x_idx] - lines_w_intersect[l1][x_idx]
                elif lineType == LineType.HORIZONTAL:
                    distance = lines_w_intersect[l2][y_idx] - lines_w_intersect[l1][y_idx]
                else:
                    raise ValueError('wrong line type')

                if abs(distance - MSDistance) < distance_merg:
                    lines_filtered.append(np.array(lines_w_intersect[l1]))
                    lines_filtered.append(np.array(lines_w_intersect[l2]))
                    break
        return np.unique(lines_filtered, axis=0)


    def updateImage(self, img):
        """
        Processes input image and aligns grid on tic-tac-toe game
        Args:
            img: input image
        Returns:
            updated image
        """
        self.logger.debug("-------------------new image")
        height, width, channels = img.shape
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        edges = cv2.Canny(gray, canny_low, canny_high, apertureSize = canny_apt_size)
        lines = cv2.HoughLines(edges, hough_rho_res, hough_theta_res, hough_vote_threshold)
        #turn up side down lines
        for l1 in range(len(lines)):
            if  lines[l1][0][rho_plot_i] < 0:
                lines[l1][0][rho_plot_i] = - lines[l1][0][rho_plot_i]
                lines[l1][0][theta_plot_i] = lines[l1][0][theta_plot_i] - np.pi
    
        ############################## Get vertical lines ########################################
        # extracting vertical lines
        vertical_lines = []        
        for l in lines:
            if l[0][rho_plot_i] != 0 and abs(l[0][theta_plot_i]) <= theta_filter:
                vertical_lines.append(l[0])
    
        # get mid-baseline intersections
        v_lines_w_intersect = self.__getLineMidIntersection(vertical_lines, LineType.VERTICAL)
        self.__drawLinesInFrame(v_lines_w_intersect, gray, (0, 0, 0))
        # mering neigbour lines
        merged_v_lines = self.__mergeLinesByDistance(v_lines_w_intersect, LineType.VERTICAL)

        # sorting lines by X coordinates 
        sorted_v_lines = self.__sortingLines(merged_v_lines, LineType.VERTICAL)
        
        # filtering out noise lines by line distances
        final_v_lines = self.__filteringLinesByDistance(sorted_v_lines, LineType.VERTICAL)
        
        # cropping matrix
        final_v_lines = final_v_lines[:,0:2]
        self.__drawLinesInFrame(final_v_lines, gray, (0, 255, 0))
        if len(final_v_lines) != grid_lines_num:
            if self.isDebug:
                self.logger.debug("Insufficient number of vertical lines: " + str(len(final_v_lines)) + " != " + str(grid_lines_num))
                
                self.logger.debug("v_lines_w_intersect:")
                for l in v_lines_w_intersect:
                    self.logger.debug(str(l))
                
                self.logger.debug("merged_v_lines:")
                for l in merged_v_lines:
                    self.logger.debug(str(l))
                
                self.logger.debug("sorted_v_lines:")
                for l in sorted_v_lines:
                    self.logger.debug(str(l))

                self.logger.debug("MSDistance: " + str(self.__getMostSignificantDistance(sorted_v_lines, LineType.VERTICAL)))

                self.logger.debug("final_v_lines:")
                for l in final_v_lines:
                    self.logger.debug(str(l))
                    coordinates = image_utils.getLineCoordinates(l[rho_plot_i], l[theta_plot_i])
                    cv2.line(img,(coordinates[0][x_plot_i], coordinates[0][y_plot_i]), (coordinates[1][x_plot_i], coordinates[1][y_plot_i]),(0,255,0),2)
                                   
                return img
            else:            
                raise Exception('Insufficient number of vertical lines')

        ############################### Get horizontal lines #######################################
        # extracting horizontal lines
        horizontal_lines = []
        for l in lines:
            if l[0][rho_plot_i] != 0 and abs(l[0][theta_plot_i] - np.pi / 2) <= theta_filter:
                horizontal_lines.append(l[0])

        # get mid-baseline intersections
        h_lines_w_intersect = self.__getLineMidIntersection(horizontal_lines, LineType.HORIZONTAL)
        self.__drawLinesInFrame(h_lines_w_intersect, gray, (0, 0, 0))
        # mering neigbour lines
        merged_h_lines = self.__mergeLinesByDistance(h_lines_w_intersect, LineType.HORIZONTAL)

        # sorting lines by Y coordinates 
        sorted_h_lines = self.__sortingLines(merged_h_lines, LineType.HORIZONTAL)

        # filtering out noise lines by line distances
        final_h_lines = self.__filteringLinesByDistance(sorted_h_lines, LineType.HORIZONTAL)

        # cropping matrix
        final_h_lines = final_h_lines[:,0:2]
        self.__drawLinesInFrame(final_h_lines, gray, (255, 255, 0))
        if len(final_h_lines) != grid_lines_num:
            if self.isDebug:
                self.logger.debug("Insufficient number of horizontal lines: " + str(len(final_h_lines)) + " != " + str(grid_lines_num))
            
                self.logger.debug("h_lines_w_intersect:")
                for l in h_lines_w_intersect:
                    self.logger.debug(str(l))
                
                self.logger.debug("merged_h_lines:")
                for l in merged_h_lines:
                    self.logger.debug(str(l))
                
                self.logger.debug("sorted_h_lines:")
                for l in sorted_h_lines:
                    self.logger.debug(str(l))

                self.logger.debug("MSDistance: " + str(self.__getMostSignificantDistance(sorted_h_lines, LineType.HORIZONTAL)))

                self.logger.debug("final_h_lines:")
                for l in final_h_lines:
                    self.logger.debug(str(l))
                    coordinates = image_utils.getLineCoordinates(l[rho_plot_i], l[theta_plot_i])
                    cv2.line(img,(coordinates[0][x_plot_i], coordinates[0][y_plot_i]), (coordinates[1][x_plot_i], coordinates[1][y_plot_i]),(0,255,0),2)
                                   
                return img
            else:            
                raise Exception('Insufficient number of horizontal lines')

        ######################### Classification & Plottig features #####################################
        gridPlot = GridPlot(final_v_lines, final_h_lines, width, height)
        board = np.full((3,3), N_i)
        for x in range(3):
            for y in range(3):
                top_left_c, bottom_right_c = gridPlot.getGridFieldCoordinates(x,y)
                # Crop out the specific field from the image
                col_img = img[top_left_c[y_plot_i]:bottom_right_c[y_plot_i], top_left_c[x_plot_i]:bottom_right_c[x_plot_i]]
                col_img_inv = (255 - col_img)
                # Extract white pixels to reduce noise
                mask = cv2.inRange(col_img, lower_white, upper_white)
                filtered_img = cv2.bitwise_and(col_img, col_img, mask=(255-mask))
                filtered_img[filtered_img > 0] = 255
                cv2.imshow('Filtered', filtered_img)
                cv2.waitKey(1)
                # Convert to grey image for classification         
                gridimg = cv2.cvtColor(filtered_img, cv2.COLOR_BGR2GRAY)       

                # Here the magic happens, as the grid-cropped image is sent to the classifier for prediction  
                prediction = self.classifierClass.predict(gridimg)
                self.logger.debug(str(x) + "-" + str(y) + ": " + str(prediction))
                if np.amax(prediction) > symbol_threshold:   
                    if np.argmax(prediction[0]) == Symbols.X.value:
                        self.logger.debug("add X")
                        board[y][x] = X_i
                    elif np.argmax(prediction[0]) == Symbols.O.value:
                        self.logger.debug("add O")
                        board[y][x] = O_i
                    elif np.argmax(prediction[0]) == Symbols.EMPTY.value:
                        self.logger.debug("Empty field")
                        board[y][x] = N_i
                else:
                    # If all 3 classifications failed, something is wrong!
                    self.logger.debug("Classification failed, add '?'")
                    board[y][x] = '?'
                        

        if self.ifPlot:
            # If plotting the grid-extraction (which we usually wants for demo-purposes), draw the detected lines in the image
            for lines_coords in gridPlot.getGridLineCoords():
                cv2.line(img,(lines_coords[0][x_plot_i], lines_coords[0][y_plot_i]), (lines_coords[1][x_plot_i], lines_coords[1][y_plot_i]),(0,255,0),2)

            # Also place dots in the grid at intersections.
            for dot_coords in gridPlot.getGridDotsCoords():
                cv2.circle(img, center=tuple(dot_coords), radius=10, color=(0,0,255), thickness=2)

            # Then for each grid (3x3), plot the detected symbol based on classified symbols (or nothing if empty). Plot '?' if something is wrong.
            for x in range(3):
                for y in range(3):
                    top_left_c, bottom_right_c = gridPlot.getGridFieldCoordinates(x,y)
                    cv2.rectangle(img, tuple(top_left_c), tuple(bottom_right_c),(255,0,0),1)

                    def getTextBottomLeftCorner(top_left_c, bottom_right_c):
                        center_x = (top_left_c[x_plot_i] + bottom_right_c[x_plot_i])/2
                        center_y = (top_left_c[y_plot_i] + bottom_right_c[y_plot_i])/2
                        
                        center_x = int(center_x) - 40
                        center_y = int(center_y) + 40
                        return center_x, center_y

                    if board[y][x] == X_i:
                        cv2.putText(img,'X',getTextBottomLeftCorner(top_left_c, bottom_right_c), cv2.FONT_HERSHEY_SIMPLEX, 4,(0,0,255),2,cv2.LINE_AA)
                    elif board[y][x] == O_i:
                        cv2.putText(img,'O',getTextBottomLeftCorner(top_left_c, bottom_right_c), cv2.FONT_HERSHEY_SIMPLEX, 4,(0,0,255),2,cv2.LINE_AA)
                    elif board[y][x] == '?':
                        cv2.putText(img,'?',getTextBottomLeftCorner(top_left_c, bottom_right_c), cv2.FONT_HERSHEY_SIMPLEX, 4,(0,0,255),2,cv2.LINE_AA)
         
        self.logger.debug("-------------------OK")
        return img, board
