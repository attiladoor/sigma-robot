# Sigma robot project

The purpose of this project is building a tic-tac-toe robot. It uses an AxiDraw xy-plotter (https://shop.evilmadscientist.com/846) and a CMOS OV9712 camera module.


## Setup
Ensure you are using Python 3.X, as some functionality is not compatible with 2.7
```bash
sudo apt-get install python3-pip
sudo pip3 install pyserial opencv-python tensorflow #or tensorflow-gpu
sudo pip3 install pillow
sudo pip3 install https://cdn.evilmadscientist.com/dl/ad/public/AxiDraw_API.zip

```

## Get started
The robot comes with a -h / --help function, which will display various options
```bash
python robot.py -h
```
To play without the camera (E.g. for robot-only testing)
```bash
python robot.py --no-camera
```
To play without the robot (E.g. for camera-only testing)
```bash
python robot.py --no-robot --camera-port=(port)
```
To play without the robot or camera (E.g. at-home testing of AI)
```bash
python robot.py --no-robot --no-camera
```

## Test Opponent-AI
You can run the PlayingAI/playAI.py script to more easily test the capabilities of the Q-table AI based on the current Q-table-files (QTable_o.txt & QTable_x.txt) rather than running the whole robot.py script (still possible but requires some additional arguments, harder to debug as well if you only want to focus on the AI part).

If weird behaviour is noted, maybe due to some weird parameter-settings when training and you have a hard time to reproduce the "perfectly learned" AI, you can replace the Q-tables directly under PlayingAI with the ones under PlayingAI/saved_qtable. These should NEVER be overwritten, only be used to overwrite the "changing" textfiles, since they are preserved from an AI that had achieved optimal playstyle, and by using these you should not be able to win (unless there's some other bug, but should not be due to faulty Q-table)

## Image processing

In order to read and recognize tic-tac-toe we should perform the following steps
* Transform image to top-down view (birds-eye view transform)
* recognizing tic-tac-toe grid
* extracting symbol fields
* recognizing symbols

Note: the camera has a major distortion that should be compensated and therefore a chess-board camera calibration is applied [link](https://gitlab.com/attiladoor/sigma-robot/tree/master/calibration)

### Top-down transform
After the init-stage of the program when the grid has been drawn, the prompt will ask the user to select 4 points in a still-image of the grid to select the corners that will act as the new edges of the top-down transformed image. With the top-left corner as first point, select each corner going clockwise, when the fourth corner has been clicked the code will continue automatically. It is recommended to select the corners a little bit outside of the actual grid corners for extra margins when transforming, such that the outer lines to not appear at the exact edges of the new image. 

### Grid detection

The grid detection is based on traditional image processing and utilizes the [OpenCV](https://opencv.org/) library.

The process consist of the following steps:
* Transform the slanted side-view to top-down perspective
* converting the input image into a grayscale image
* performing [Canny](https://en.wikipedia.org/wiki/Canny_edge_detector) edge detection
* running [Hough-line](https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_houghlines/py_houghlines.html) transformation that would provide us the *rho* and *theta* parameters of the detected lines

![alt text](documentation/raw-lines.png)
* extracting vertical and horizontal lines: A filter parameter is compared with the *theta* parameter:
    * Those that are around 0 should be vertical meanwhile those that are around pi/2 should be horizontal

![alt text](documentation/raw-horizontal.png)

![alt text](documentation/raw-vertical.png)
* There are many duplicated lines and that should be reduced, that's why an intersection point is calculated respectively an imaginary baseline

![alt text](documentation/mid-intersect.png)

* Lines are merged according to their baseline intersections

![alt text](documentation/merged-lines.png)

* for easier handling the lines are sorted according to their baseline intersections: vertical lines are by X coordinates meanwhile horizontal lines by Y coordinates
* for more robust algorithm the unwanted noise lines are filtered out by the distances between lines according to the following procedure:
    * distances between neighbor lines are calculated
    * by merging and weighthing them, we can select the one that occures the most (=MSDistance)
    * we calculate the distance between all lines and take away only those that are in MSDistance from another line (not necessarily neighbor lines -> there could be a noise line between them)

![alt text](documentation/detected-with-noise-line.png)

![alt text](documentation/detected-with-noise-line2.png)

* At the and a grid is aligned on the image for demo purposes

![alt text](documentation/detected.png)

* in the next step, the symbols are classified by a Convolutional Neural Network (see TODO: add classifier documentation) 

![alt text](documentation/detected-symbols.png)
