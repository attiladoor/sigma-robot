import sys
import os
from pyaxidraw import axidraw
import numpy as np
import math
sys.path.append(os.path.join(os.path.dirname(__file__), '../misc'))
from params import * # Imports general parameters of the project, such as ranges for the grid-size drawn by the plotter (maxX/maxY) and offsets

class Plotter:
    def __init__(self, logger):
        self.plotter = axidraw.AxiDraw() # Creates an Axidraw-API object, refer to Axidraw documentation for details
        self.plotter.interactive()
        # Define speends for pen-movement up/down, these can be changed if the plotter seems too fast/slow
        self.plotter.options.speed_pendown = 50
        self.plotter.options.const_speed = True
        self.plotter.options.pen_delay_down = 100
        logger.debug('Plotter handle created.')   

    def connect(self, logger):        
        self.plotter.connect() # Will connect to the Axidraw plotter through USB cable automatically, will throw expection if connection fails
        logger.debug('Connected to plotter.')     

    def height_config(self):
        # This function will move the lotter-head to the left of the grid in x-position, and lift down the head to 'line'-level
        self.plotter.moveto(maxX_ + 1, 0)
        self.plotter.line(0, 0)
        input('Adjust the pen such that the tip touches the paper properly then press enter.')
        # Move back to origin
        self.plotter.moveto(0, 0)
        
    def draw_grid(self):
        # Move to origin if in any other position
        self.plotter.moveto(0, 0)

        # Draw outer square
        self.plotter.lineto(0, maxY_)
        self.plotter.lineto(maxX_, maxY_)
        self.plotter.lineto(maxX_, 0)
        self.plotter.lineto(0, 0)

        # First vertical line
        self.plotter.moveto(maxX_/3, 0)
        self.plotter.lineto(maxX_/3, maxY_)
        # Second vertical line
        self.plotter.moveto(2*maxX_/3, 0)
        self.plotter.lineto(2*maxX_/3, maxY_)
        # First horizontal line
        self.plotter.moveto(0, maxY_/3)
        self.plotter.lineto(maxX_, maxY_/3)
        # Second horizontal line
        self.plotter.moveto(0, 2*maxY_/3)
        self.plotter.lineto(maxX_, 2*maxY_/3)

       # Move to pen-down origin    
        self.plotter.moveto(0, 0)

    def draw_X_at(self, x_grid, y_grid):
        # Get upper-left corner of selected grid
        upper_corner_x = (1 - 1 / 3 * (x_grid - 1)) * maxX_ - draw_X_offset_x
        upper_corner_y = (1 - 1 / 3 * (y_grid - 1)) * maxY_ - draw_X_offset_y

        # Draw x according to grid size and offset-values
        self.plotter.moveto(upper_corner_x, upper_corner_y)
        self.plotter.line(-maxX_ / 3 + 2 * draw_X_offset_x, -maxY_ / 3 + 2 * draw_X_offset_y)
        self.plotter.move(0, maxY_ / 3 - 2 * draw_X_offset_y)
        self.plotter.line(maxX_ / 3 - 2 * draw_X_offset_x, -maxY_ / 3 + 2 * draw_X_offset_y)

        # Move to pen-down origin    
        self.plotter.moveto(0, 0)

    def draw_O_at(self, x_grid, y_grid):
        # Get centre-point of selected grid
        center_x = (1 - 1 / 3 * (x_grid - 1)) * maxX_ - maxX_ / 6
        center_y = (1 - 1 / 3 * (y_grid - 1)) * maxY_ - maxY_ / 6

        # Get circle-coords based on radius and resolution
        theta = np.linspace(0, 2 * math.pi, O_resolution_)
        circle_x = O_radius_ * maxX_ / 6 * np.cos(theta) + center_x
        circle_y = O_radius_ * maxY_ / 6 * np.sin(theta) + center_y

        # Plot circle-coords
        self.plotter.moveto(circle_x[0], circle_y[0])
        for i in range(len(theta) - 1):
            self.plotter.lineto(circle_x[i+1], circle_y[i+1])

        # Move to pen-down origin    
        self.plotter.moveto(0, 0)

    def disconnect(self, logger):
        # This function will disconnect the plotter more cleanly than simply breaking the connection, but I've never seen any consequence of simply plugging it out after use,
        # so this function is currently unused in the 'robot.py' main script, but could be added in the shut-down stages

        # Move to pen-down origin    
        self.plotter.moveto(0, 0)

        self.plotter.disconnect()    
        logger.debug('Disconnected from plotter.')   