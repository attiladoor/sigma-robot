# How to run the classifier:
Currently it has only been verified to work for CPU-tensorflow.
For intalling the dependencies, run:

```bash
sudo pip3 install matplotlib pillow scipy pandas tensorflow=1.13.1 #or tensorflow-gpu
```

For executing the training and verification, run:

```bash
python3 train_classifier.py
```

## If running Classifier.py as main:
    The dataset is extracted from a zip file, store in the "data" folder. The model output is stored in the "out" folder.


    This is also where the checkpoint-file of the model will be stored.
    Then it should just be to run the script using a relevant interpreter containing all the imports.
    Depending on whether or not you want to load checkpoint-model and continue training, or start again, set the relevant flags "loadcheckpoint" and "continuetraining"