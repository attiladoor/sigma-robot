from __future__ import absolute_import, division, print_function

import matplotlib.pyplot as plt
# Helper libraries
import numpy as np
import os
# TensorFlow and tf.keras
import tensorflow as tf
from PIL import Image
from os import listdir
from tensorflow import keras
import pandas
import zipfile
import random
import cv2

DATA_PATH = os.path.join(os.path.dirname(__file__), './data/')
OUTPUT_PATH = os.path.join(os.path.dirname(__file__), './out/')
no_of_empty_train_images = 15000
no_of_empty_test_images = 15000

def unzip_data_on_demand():

    if not os.path.exists(DATA_PATH + "Train") and not os.path.exists(DATA_PATH + "Test"):
        print("train and test datasets not found. Extracting files...")
        zip_ref = zipfile.ZipFile(DATA_PATH + "data_set.zip", 'r')
        zip_ref.extractall(DATA_PATH)
        zip_ref.close()
    else:
        print("train and test dataset found")

def load_data():
    TrainingPath = DATA_PATH + "Train/"
    TestPath =  DATA_PATH +  "Test/"
    checkpoint_path = OUTPUT_PATH + "model.hdf5"
    TrainFiles = []
    TestFiles = []
    print('Loading files...')
    #Finding all files and putting them in lists
    for dirName, subDirList, fileList in os.walk(TrainingPath):
        for fname in fileList:
            filename, extension = os.path.splitext(TrainingPath + fname)
            if extension == '.jpg' or extension == '.png':
                TrainFiles.append(fname)
        break

    for dirName, subDirList, fileList in os.walk(TestPath):
        for fname in fileList:
            filename, extension = os.path.splitext(TestPath + fname)
            if extension == '.jpg' or extension == '.png':
                TestFiles.append(fname)
        break

    print('Shuffle files...')
    #Convert into numpy to randomly shuffle
    np_Training = np.array(TrainFiles)
    for i in range(no_of_empty_train_images):
        np_Training = np.append(np_Training, 'empty')
    np_Test = np.array(TestFiles)
    for i in range(no_of_empty_test_images):
        np_Test = np.append(np_Test, 'empty')
    np.random.shuffle(np_Training)
    np.random.shuffle(np_Training)
    np.random.shuffle(np_Test)
    np.random.shuffle(np_Test)

    np_TrainLabels = np.zeros(len(np_Training))
    np_TestLabels = np.zeros(len(np_Test))

    i = 0
    print('Creating classifying labels...')
    for filename in np_Training:
        np_TrainLabels[i] = 2
        if 'O' in filename:
            np_TrainLabels[i] = 1
        if 'X' in filename:
            np_TrainLabels[i] = 0
        i += 1
    i = 0
    for filename in np_Test:
        np_TestLabels[i] = 2
        if 'O' in filename:
            np_TestLabels[i] = 1
        if 'X' in filename:
            np_TestLabels[i] = 0
        i += 1

    #Load one non-emty sample to get correct size of image in numpy array
    n = 0
    while np_Training[n] == 'empty':
        n += 1
    im = Image.open(TrainingPath + np_Training[n])
    np_im = np.array(im)
    np_TrainData = np.empty([len(np_Training), np_im.shape[0], np_im.shape[1]])
    np_TestData = np.empty([len(np_Test), np_im.shape[0], np_im.shape[1]])

    i = 0
    print('Extracting data from files..')
    for file in np_Training:
        if file == 'empty':
            np_TrainData[i] = np.zeros(np_im.shape)
        else:    
            np_TrainData[i] = np.array(Image.open(TrainingPath + file))
        i += 1

    i = 0
    for file in np_Test:
        if file == 'empty':
            np_TestData[i] = np.zeros(np_im.shape)
        else:    
            np_TestData[i] = np.array(Image.open(TestPath + file)) 
        i += 1

    np_TrainData = np_TrainData / 255.0
    np_TestData = np_TestData / 255.0

    #np_TestLabels = tf.keras.utils.to_categorical(np_TestLabels)
    #np_TrainLabels = tf.keras.utils.to_categorical(np_TrainLabels)

    np_TestData_Reshaped = tf.reshape(np_TestData, [-1, np_im.shape[0], np_im.shape[1], 1])
    np_TrainData_Reshaped = tf.reshape(np_TrainData, [-1, np_im.shape[0], np_im.shape[1], 1])

    return np_TrainData_Reshaped, np_TestData_Reshaped, np_TrainLabels, np_TestLabels, np_TrainData, np_TestData,\
           np_im, checkpoint_path


def createmodel(checkpoint_path, im):
    #Setup model and callback

    #Fully-connected dense classifier
    # model = keras.Sequential([
    #    keras.layers.Flatten(input_shape=(np_im.shape[0], np_im.shape[1])),
    #    keras.layers.Dense(128, activation=tf.nn.relu),
    #    keras.layers.Dense(2, activation=tf.nn.softmax)
    #])
    print('Creating model.....')
    #CNN network
    model = keras.Sequential()
    model.add(keras.layers.Conv2D(32, kernel_size=(5, 5), strides=(1, 1),
                     activation=tf.nn.relu, input_shape=(im.shape[0], im.shape[1], 1)))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(keras.layers.Conv2D(64, (5, 5), activation=tf.nn.relu))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(keras.layers.Flatten())
    model.add(keras.layers.Dense(1024, activation=tf.nn.relu))
    model.add(keras.layers.Dense(3, activation=tf.nn.softmax))
    print('Model created!')

    model.compile(optimizer='adam',
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    checkpoint_callback = keras.callbacks.ModelCheckpoint(checkpoint_path,
                                               save_weights_only=True, verbose=1)
    return model, checkpoint_callback

def add_pixel_noise(img):
    '''Add random noise to an image'''
    VARIABILITY = 0 / 255
    deviation = VARIABILITY*random.random()
    noise = np.random.normal(0, deviation, img.shape)
    img += noise
    img = np.clip(img, 0., 1.)
    cv2.imshow('noised', img*255)
    cv2.waitKey(1)
    #ret,b_img = cv2.threshold(img, 50, 255, cv2.THRESH_BINARY)
    return img

def train(model, TrainData_Reshaped, TrainLabels, checkpoint_callback, steps, batchsize, epochs):
    print('Training model...')
    #Zooming and rotational augmentation
    aug = keras.preprocessing.image.ImageDataGenerator(rotation_range=30, zoom_range=0.5, preprocessing_function=add_pixel_noise)
    model.fit_generator(aug.flow(TrainData_Reshaped, TrainLabels, batch_size=batchsize), epochs=epochs, verbose=1,
              steps_per_epoch=steps, callbacks=[checkpoint_callback], shuffle=True)
    return model


def evaluate(model, TestData_Reshaped, TestLabels, steps, batchsize):
    #No augmentation, just 1 to 1 generator
    aug = keras.preprocessing.image.ImageDataGenerator()
    loss, acc = model.evaluate_generator(aug.flow(TestData_Reshaped, TestLabels, batch_size=batchsize), steps=steps)
    print('Test accuracy:', acc)
    return loss, acc


def predict(model, PredictData_Reshaped, steps):
    predictions = model.predict(PredictData_Reshaped, steps=steps)
    return predictions


def loadmodelfromcheckpoint(model, checkpath):
    print("Loading model from checkpoint")
    model.load_weights(checkpath)
    return model


if __name__ == "__main__":
    # Main script


    unzip_data_on_demand()
    #Load the data
    TrainData_Res, TestData_Res, TrainLabels, TestLabels, TrainData, TestData, exImage, check_path = load_data()

    sess = tf.compat.v1.Session()
    with sess.as_default():
        TrainDataAsNP = TrainData_Res.numpy()
        TestDataAsNP = TestData_Res.numpy()

    #Create CNN model
    cnnModel, callback = createmodel(check_path, exImage)

    #Specify steps for training, testing and predicts
    batch_size = 32
    trainSteps = int(TrainData_Res.shape[0]) // batch_size
    testSteps = int(TestData_Res.shape[0]) // batch_size
    #predictSteps = int(TestData_Res.shape[0]) // batch_size
    epochs = 2

    #trainSteps = 64
    #testSteps = 200
    predictSteps = 10

    loadcheckpoint = True
    continueTraining = False
    #Train model if checkpoint is not existing
    if os.path.isfile(check_path) and loadcheckpoint:
        loadmodelfromcheckpoint(cnnModel, check_path)
        if continueTraining:
            cnnModel = train(cnnModel, TrainDataAsNP, TrainLabels, callback, trainSteps, batch_size, epochs)
    else:
        cnnModel = train(cnnModel, TrainDataAsNP, TrainLabels, callback, trainSteps, batch_size, epochs)
    cnnModel.save(check_path)

    #Evaluate
    #evaluate(cnnModel, TestDataAsNP, TestLabels, testSteps, batch_size)

    #Make a test-prediction
    n = 0
    for Testdata in TestData:
        if TestLabels[n] == 0:
            break
        n = n + 1
    
    for i in range(predictSteps):
        # Currently using testdata for testing prediction
        desiredLabel = i % 3
        randomIndex = random.choice(range(len(TestData_Res)))
        while TestLabels[randomIndex]  != desiredLabel:
            randomIndex = random.choice(range(len(TestData_Res)))
        singleData = tf.reshape(TestData_Res[randomIndex], [1, 28, 28, 1])
        predictions = predict(cnnModel, singleData, 1)
        symbols = ['X', 'O', 'Empty']
        plt.figure()
        plt.imshow(TestData[randomIndex])
        plt.show()
        print('Prediction: ', symbols[np.argmax(predictions[0])])
        print('True: ', symbols[int(TestLabels[randomIndex])])
        #keras.utils.plot_model(cnnModel, to_file=mainPath+"model.png")
        print(predictions)
        #PlotFrame = pandas.DataFrame({'predictions': predictions[0]}, index=symbols)
        #plt.figure()
        #ax = PlotFrame.predictions.plot(kind='bar')
        plt.show()

