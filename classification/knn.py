import numpy as np
import os
from PIL import Image

import cv2 as cv
from matplotlib import pyplot as plt

#Paths
mainPath = "/home/attila/Downloads/symbols/O/"
files = []
#Finding all files and putting them in lists
for dirName, subDirList, fileList in os.walk(mainPath):
    for fname in fileList:
        filename, extension = os.path.splitext(mainPath + fname)
        if extension == '.jpg' or extension == '.png':
            files.append(fname)
    break

#Convert into numpy to randomly shuffle
np_files = np.array(files)
np.random.shuffle(np_files)

np_Test = np_files[0:10000]
np_Training = np_files[10000:]

np_TrainLabels = np.zeros(len(np_Training))
np_TestLabels = np.ones(len(np_Test))

i = 0
for filename in np_Training:
    if 'O' in filename:
        np_TrainLabels[i] = 1
    else:
        np_TrainLabels[i] = 0
    i += 1
i = 0
for filename in np_Test:
    if 'O' in filename:
        np_TestLabels[i] = 1
    else:
        np_TestLabels[i] = 0
    i += 1

#Load one sample to get correct size of image in numpy array
im = Image.open(mainPath + np_Training[0])
np_im = np.array(im)

np_TrainData = np.empty([len(np_Training), np_im.shape[0] * np_im.shape[1]])
np_TestData = np.empty([len(np_Test), np_im.shape[0] * np_im.shape[1]])

i = 0
for file in np_Training:
    np_TrainData[i] = np.array(Image.open(mainPath + file)).flatten()

i = 0
for file in np_Test:
    np_TestData[i] = np.array(Image.open(mainPath + file)).flatten()
    i += 1

knn = cv.ml.KNearest_create()
knn.train(np_TrainData.astype(np.float32), cv.ml.ROW_SAMPLE, np_TrainLabels.astype(np.float32))
ret,result,neighbours,dist = knn.findNearest(np_TestData.astype(np.float32),k=2)

correct = 0
for i in range(result.size):
    if result[i] == np_TestLabels[i]:
        correct+=1

accuracy = correct*100.0/result.size
print( accuracy )