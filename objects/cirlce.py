import math
import numpy as np

ANGLE_RESOLUTION = 2 #degree
DATA_POINTS = int(360 / ANGLE_RESOLUTION)

class Circle:
    def __init__(self, origo_x, origo_y, radius):
        self.origo_x = origo_x
        self.origo_y = origo_y
        self.radius = radius

    def getCoordinates(self, z_down, z_up):
        
        coordinates = np.zeros((0, 3))
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + self.radius, self.origo_y, z_up]).reshape(1, 3)), axis=0) 

        for alfa in range(DATA_POINTS + 1):
            xn = (math.cos(math.radians(alfa * ANGLE_RESOLUTION)) * self.radius) + self.origo_x
            yn = (math.sin(math.radians(alfa * ANGLE_RESOLUTION)) * self.radius) + self.origo_y

            coordinates = np.concatenate((coordinates, np.array([xn, yn, z_down]).reshape(1, 3)), axis=0) 

        coordinates = np.concatenate((coordinates, np.array([self.origo_x + self.radius, self.origo_y, z_up]).reshape(1, 3)), axis=0) 
        return coordinates    
