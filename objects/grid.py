import numpy as np

class Grid:
    def __init__(self, origo_x, origo_y, size_x, size_y):
        self.origo_x = origo_x
        self.origo_y = origo_y
        self.size_x = size_x
        self.size_y = size_y

    def getCoordinates(self, z_down, z_up, frame=False):
        
        coordinates = np.zeros((0, 3))

        if frame:
            coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y - (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 
            coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y - (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
            coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y - (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
            coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y + (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
            coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y + (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
            coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y - (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
            coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y - (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 

        #line
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y - (self.size_y / 6), z_up]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y - (self.size_y / 6), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y - (self.size_y / 6), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y - (self.size_y / 6), z_up]).reshape(1, 3)), axis=0) 

        #line
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y + (self.size_y / 6), z_up]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y + (self.size_y / 6), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y + (self.size_y / 6), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y + (self.size_y / 6), z_up]).reshape(1, 3)), axis=0) 
        
        #line
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 6), self.origo_y + (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 6), self.origo_y + (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 6), self.origo_y - (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 6), self.origo_y - (self.size_y / 2), z_up]).reshape(1, 3)), axis=0)

        #line
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 6), self.origo_y - (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 6), self.origo_y - (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 6), self.origo_y + (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 6), self.origo_y + (self.size_y / 2), z_up]).reshape(1, 3)), axis=0)

        return coordinates    


    def getFieldPosition(self, row, column):

        x = self.origo_x + ((column -2) * self.size_x / 3)
        y = self.origo_y - ((row - 2) * self.size_y / 3)
        return x, y

    def getVerticalLinePosition(self, z_up, z_down, pos):

        pos -= 2
        coordinates = np.zeros((0, 3))
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + pos * (self.size_x / 3), self.origo_y + (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + pos * (self.size_x / 3), self.origo_y + (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + pos * (self.size_x / 3), self.origo_y - (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + pos * (self.size_x / 3), self.origo_y - (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 
        return coordinates

    def getHorizontalLinePosition(self, z_up, z_down, pos):

        pos = pos - 2
        coordinates = np.zeros((0, 3))
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y - pos * (self.size_y / 3), z_up]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y - pos * (self.size_y / 3), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y - pos * (self.size_y / 3), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y - pos * (self.size_y / 3), z_up]).reshape(1, 3)), axis=0) 
        return coordinates

    def getDownCrossPosition(self, z_up, z_down):

        coordinates = np.zeros((0, 3))
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y + (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y + (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y - (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y - (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 
        return coordinates

    def getUpCrossPosition(self, z_up, z_down):

        coordinates = np.zeros((0, 3))
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y + (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y + (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y - (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y - (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 
        return coordinates