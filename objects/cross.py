import numpy as np

class Cross:
    def __init__(self, origo_x, origo_y, size_x, size_y):
        self.origo_x = origo_x
        self.origo_y = origo_y
        self.size_x = size_x
        self.size_y = size_y

    def getCoordinates(self, z_down, z_up):
        
        coordinates = np.zeros((0, 3))

        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y - (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y - (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y + (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y + (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y + (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x + (self.size_x / 2), self.origo_y + (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y - (self.size_y / 2), z_down]).reshape(1, 3)), axis=0) 
        coordinates = np.concatenate((coordinates, np.array([self.origo_x - (self.size_x / 2), self.origo_y - (self.size_y / 2), z_up]).reshape(1, 3)), axis=0) 
        return coordinates    
