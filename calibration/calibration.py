import numpy as np
import cv2 as cv
import glob

CHESS_X = 6
CHESS_Y = 9

# termination criteria
criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((CHESS_X * CHESS_Y,3), np.float32)
objp[:,:2] = np.mgrid[0:CHESS_Y, 0:CHESS_X].T.reshape(-1,2)
# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.
images = glob.glob('./images/*')
for fname in images:
    img = cv.imread(fname)
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    # Find the chess board corners
    ret, corners = cv.findChessboardCorners(gray, (CHESS_Y, CHESS_X), None)
    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)
        corners2 = cv.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
        imgpoints.append(corners)
        # Draw and display the corners
        cv.drawChessboardCorners(img, (CHESS_Y, CHESS_X), corners2, ret)
        cv.imshow('img', img)
        cv.waitKey(500)
    else:
        print("Chessboard not found: " + fname)	
cv.destroyAllWindows()

# calibration
ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

# camera matrix with new image
img = cv.imread('calibtest_in.png')
h,  w = img.shape[:2]
newcameramtx, roi = cv.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))

# undistort
dst = cv.undistort(img, mtx, dist, None, newcameramtx)
# crop the image
x, y, w, h = roi
dst = dst[y:y+h, x:x+w]
cv.imwrite('calibtest_out.png', dst)

np.savetxt("calib_matrix.txt", mtx)
np.savetxt("calib_dist.txt", dist)
np.savetxt("calib_cammatrix.txt", newcameramtx)