import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), './image'))
sys.path.append(os.path.join(os.path.dirname(__file__), './objects'))
sys.path.append(os.path.join(os.path.dirname(__file__), './misc'))
sys.path.append(os.path.join(os.path.dirname(__file__), './uArm-Python-SDK'))
sys.path.append(os.path.join(os.path.dirname(__file__), './xy_plotter'))
sys.path.append(os.path.join(os.path.dirname(__file__), './PlayingAI'))
import argparse
import cv2

# Import custom classes and functions
from image_handler import ImageHandler
from logger import Logger
from Plotter import Plotter
from AI import AI
from Board import Board
from params import * # Gives us X_i, O_i, N_i and other parameters

# Initiate global plotter-object, to be assigned in ConnectToRobot below
plotter = None

def ConnectToRobot(logger):
    # Create plotter-object and connect (requires Plotter to be connected via USB to computer)
    plotter = Plotter(logger)
    plotter.connect(logger)
    return plotter

def drawSymbolInGrid(symbol, x, y):
    # Will draw the specified symbol in grid specified by integers x and y (only X and O implemented) and call the corresponding specific sub-functions 
    if symbol == X_i:
        plotter.draw_X_at(x, y)
    if symbol == O_i:
        plotter.draw_O_at(x, y)
    if symbol != O_i and symbol != X_i:
        print('Symbol ' + str(symbol) + ' not implemented for printing!')
        raise ValueError() 
        
def capturePlayerInput(playerSymbol, board, robotConnected):
    # Helper function for capturing prompt-input of player when not playing with camera, will ask for column and then row
    # If robot is connected (== plotter) then it will also draw this symbol in the actual grid
    column = int(input(playerSymbol + " player next. Choose column (1-3): "))
    if column < 1 or 3 < column:
        print("invalid argument")
        raise ValueError()

    line = int(input("Choose line (1-3): "))

    if line < 1 or 3 < line:
        print("invalid argument")
        raise ValueError()
    
    # After getting column and row, convert to linear position (0-8) and set tile in board-object 
    lin_pos = (line - 1) * 3 + (column - 1)
    success = board.setTile(lin_pos, playerSymbol)
    if not success:
        raise ValueError("Field is not empty")
    if robotConnected:
        drawSymbolInGrid(playerSymbol, column, line)

if __name__ == "__main__":
    ### Main ###
    # First, parse arguments from command line
    parser = argparse.ArgumentParser(description='Process some cmd arguments.')
    # Warning: It is entirely possible to use the camera without the plotter, and draw the grid by hand, but it is not recommended.
    # The reason is that the robot draws very nice and straight lines, a human might not draw a grid as nicely. 
    # But it is possible, so we allow it. Also, this is a "Man vs Machine" scenario, so it is of course the whole point with having the plotter acting as the AI-actuator.
    parser.add_argument('--no-robot', action='store_true', help='Disabling connection to robot arm')
    parser.add_argument('--no-camera', action='store_true', help='Disabling connection to camera')
    parser.add_argument('--debug', action='store_true', help='Enabling debug messages')
    parser.add_argument('--camera-port', help='camera port as integer, eg.: 0 or 1', default='0')

    args = parser.parse_args()

    # Create logger object (will print debug/error messages if enabled)
    logger = Logger(args.debug)

    # SETUP
    if not args.no_robot:
        # If robot (== plotter) is to be used, create plotter-object and connect, will throw expection if plotter is not found.
        plotter = ConnectToRobot(logger)
        config = input("Would you like to configure the pen-height? type: y/any button: ")
        if config == "y":
            # If user wants to configure height of the pen (to avoid "hovering" when drawing or drawing too hard), then plotter will move to the side to allow adjustment
            plotter.height_config()
        plotGrid = input("Would you like to re-draw the grid? type: y/any button: ")
        if plotGrid == "y":
            # If user wants to re-draw the grid on the paper (Usually the case, unless starting from a completely un-filled grid from previous run)
            plotter.draw_grid()
  
    # If camera is used, then initiate Image-handler object and using the selected camera port (if you are on a laptop with a webcam it should be port 2, it should be fairly obvious if it selects the right one)
    imageHandler = None
    if not args.no_camera:
        imageHandler = ImageHandler(args.debug, int(args.camera_port))
        imageHandler.start() # Will start the parallell thread of the image handler

    nextSymbol = None
    playerSymbol = None
    AISymbol = None

    # Initialize board-object that handles game-state and used by the AI-class
    symbols = [N_i, X_i, O_i]
    board = Board(symbols)
    
    # "Play again" loop
    replay = False 
    while(True):
        # If replay, ask again to draw grid (otherwise the prompt will appear twice at the start of the program)
        # Most likely, the answer to this prompt will be "y", since we need a new grid after a fully executed game
        if not args.no_robot and replay:
            plotGrid = input("Would you like to re-draw the grid? type: y/any button: ")
            if plotGrid == "y":
                plotter.draw_grid()
        
        # Symbol selection, will prompt user to select symbol and assign AI the other symbol
        playerStarts = input("Would you like to start (play as X)? type: y/n: ")
        if playerStarts != "y" and playerStarts != "n":
            logger.fail("invalid symbol")
            if not args.no_camera:
                # Stop image handler only if it was selected through arguments
                imageHandler.stop()
                imageHandler.join()
            exit(1)     
        else:
            # Select symbols based on player-decision to start or not
            if playerStarts == "y":
                playerSymbol = X_i
                AISymbol = O_i
                nextSymbol = playerSymbol                
            else:
                playerSymbol = O_i
                AISymbol = X_i   
                nextSymbol = AISymbol      
        # Add debug-print of symbol selection                         
        logger.debug("player symbol: " + str(playerSymbol))
        logger.debug("ai symbol: " + str(AISymbol))
        # Create AI-object using discount-rate == 0.8, epsilon (randomness) == 0, learning-rate == 0.4 and corresponding symbol.
        # The symbol selected will determine which Q-table is loaded (see class for details), here we assume the AI is fully trained and the table known
        Ai = AI(board, 0.8, 0, 0.4, AISymbol)

        # Perform game
        while(True):
            # Debug-print of current symbol in play
            logger.debug("next player: " + str(nextSymbol))

            # If AI's turn, perform action and save state-action pair for on-line training (should not be needed with a fully-learned AI but won't hurt)
            # This state-action saving actually enables us to play with a completely un-trained AI (i.e. no previous Q-table file) and let it learn playing vs actual humans,
            # rather than vs a random opponent as in the automatic learning script. This could be a fun experience for a demonstration for example, but might take a lot of play-throughs to start seeing differences in playstyle
            # EDIT: State-action is saved but not actually used since we have no callback to the 'updateQTable' function at the end of the game as well as no reward. If this is desired, refer to the 'playAI' or 'trainAI' scripts
            if nextSymbol == AISymbol:
                [state, action, row, col] = Ai.performAction()
                Ai.addStateAction(state, action)
                # If robot is connected, draw the performed action on the paper
                if not args.no_robot:
                    drawSymbolInGrid(AISymbol, col + 1, row + 1)
                logger.debug("AI next step is: " + str(row) + ":" + str(col))
                nextSymbol = playerSymbol # Next turn is player

            else: # Player's turn
                # If no camera connected, capture player-input through prompt and catch invalid moves.
                # Expections will cause the loop to continue and come back to ask for player input again, in case an invalid move was made
                if args.no_camera:
                    try:
                        capturePlayerInput(playerSymbol, board, not args.no_robot)
                        nextSymbol = AISymbol
                    except Exception as e:
                        logger.system("invalid input")
                        logger.debug(str(e)) # Debug which move was invalid, either out of bounds or non-empty tile
                        continue
                else:
                    # If camera is used, then we expect the player to instead draw their symbol in the grid and then the Image-handler should detect the new state
                    input("Draw your move and press enter when ready.") # To only capture the new image-state when ready
                    nextSymbol = AISymbol
                    gameStatus = imageHandler.getStableStatus() # This function was added to attempt to stabilize the state, such that occasional image-flickers do not suddenly change the state between samples
                    # Update the board-object with the corresponding new-state, so that the AI will receive the correct state when making their move
                    tiles = gameStatus.flatten() # Convert to board-convention
                    board.updateState(tiles) # Here we assume the gameStatus received from imageHandler is the absolute truth, errors in the image-processing might cause a huge impact here if improper settings

            # Print board in command-prompt for visualization, also only visual aid if no camera / robot is used.
            print('-------- Current Status -------------')    
            board.writeBoard() # Calls the board's write-function

            # Determine whether or not the game is over and prints out the outcome, then breaks the 'game-loop'
            # Here we would also determine reward (positive/negative) depending on outcome from the AI's point of view, if we were to use on-line learning
            if board.isDraw():
                logger.system("Game over, draw!")
                break
            elif board.isWinner(playerSymbol):
                logger.system("Game over, winner: " + playerSymbol + '!')
                break
            elif board.isWinner(AISymbol):
                logger.system("Game over, winner: " + AISymbol + '!')
                break
        # Ask user if they wish to play again, and resets the board-state to empty grid
        playAgain = input('Play again? (y / any key)')
        board.resetBoard()
        replay = True
        # If no replay, break and end the main-loop
        if playAgain != 'y':
            break

    # When everything done, stop image-handling thread and close all (possibly) open windows, then exit
    cv2.destroyAllWindows()
    if not args.no_camera:    
        imageHandler.stop()
        imageHandler.join()
    exit(0)  

