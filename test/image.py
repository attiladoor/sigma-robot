import cv2
import numpy as np

rho_e = 10
theta_e = 0.02
distance_e = 0.05 #percentage

img = cv2.imread('./pictures/pic2.jpg')
height, width, channels = img.shape

def line_intersection(line1, line2):
    
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1]) 

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
       raise Exception('lines do not intersect')

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / div
    y = det(d, ydiff) / div

    if x < 0 or y < 0 or x > width or y > height:
        raise Exception('lines do not intersect')

    return int(x), int(y)

def get_line_coordinates(rho, theta):

    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a*rho
    y0 = b*rho
    x1 = int(x0 + 1000*(-b))
    y1 = int(y0 + 1000*(a))
    x2 = int(x0 - 1000*(-b))
    y2 = int(y0 - 1000*(a))
    return [[x1, y1], [x2 ,y2]]


if __name__ == "__main__":

    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray,50,150,apertureSize = 3)

    lines = cv2.HoughLines(edges,1,np.pi/180,150)
    lines_merged = []

    #mering lines
    for l1 in range(len(lines)):
        for l2 in range(l1 + 1, len(lines)):
            if abs(lines[l1][0][0] - lines[l2][0][0]) < rho_e and abs(lines[l1][0][1] - lines[l2][0][1]) < theta_e:
                lines[l2][0][0] = (lines[l2][0][0] + lines[l2][0][0]) / 2
                lines[l2][0][1] = (lines[l2][0][1] + lines[l2][0][1]) / 2
                lines[l1][0][0] = 0
                lines[l1][0][1] = 0

    #taking non zero values
    for d2 in lines:
        if d2[0][0] != 0 and d2[0][1] !=0:
            lines_merged.append(d2[0])

    #getting intersections
    dots = []
    for l1 in range(len(lines_merged)):

        lines_coords = get_line_coordinates(lines_merged[l1][0], lines_merged[l1][1])
        #draw lines
        cv2.line(img,(lines_coords[0][0], lines_coords[0][1]), (lines_coords[1][0], lines_coords[1][1]),(0,255,0),2)

        for l2 in range(l1, len(lines_merged)):
            try:
                dot = line_intersection(get_line_coordinates(lines_merged[l1][0], lines_merged[l1][1]), 
                                        get_line_coordinates(lines_merged[l2][0], lines_merged[l2][1]))
                dots.append(dot)
                # mark dots
                cv2.circle(img, center=dot, radius=10, color=(0,0,255), thickness=2)
            except Exception as e:
                pass

    #reveal high ranked dots 
    trusted_dots = []
    for i in range(len(dots)):
        # calculate distances matrix
        distances = []
        for j in range(len(dots)):
            if i != j:
                distance = np.sqrt(np.square(dots[i][0] - dots[j][0]) + np.square(dots[i][1] - dots[j][1]))
                distances.append(distance)
        
        # merge distances by similar ones
        weights = np.ones(len(distances))
        for d1 in range(len(distances)):
            for d2 in range(d1 + 1, len(distances)):

                if abs(distances[d1] - distances[d2]) <= (distance_e * distances[d1]): 
                    distances[d2] = ((weights[d1] * distances[d1]) + (weights[d2] * distances[d2])) / (weights[d1] + weights[d2])
                    weights[d2] += weights[d1]
                    weights[d1] = 0
                    break

        for w in range(len(weights)):
            if weights[w] >= 4:
               trusted_dots.append([dots[i], distances[w]])  

    #print(distances)

    cv2.imwrite('houghlines3.jpg',img)
