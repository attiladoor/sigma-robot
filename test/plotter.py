import numpy as np
from PIL import Image, ImageDraw
from enum import Enum

UPSCALE = 5

class p(Enum):
    x = 1
    y = 0
    z = 2

class Plotter:

    def __init__(self, size_x, size_y, z_down, z_up):
        self.size_x = size_x
        self.size_y = size_y
        self.z_down = z_down
        self.z_up = z_up
        self.image = Image.new('1', (size_y, size_x), 1) 
        self.pointer = [0, 0, z_up]
    
    def set_position(self, x, y, z, speed):

        if self.pointer[p.z.value] == self.z_down:
            draw = ImageDraw.Draw(self.image)
            draw.line((y, x, self.pointer[p.y.value], self.pointer[p.x.value]))

        self.pointer = [y, x, z]

    def show_image(self):
        self.image.resize((self.size_y * UPSCALE, self.size_x * UPSCALE) ,1).show()

