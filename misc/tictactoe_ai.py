import sys
import os
import random
import numpy as np
from params import *

n_i = N_i

# 2022-06-14: Fairly sure this class is unused in the robot-project, it is part of legacy code in the old logical-opponent implementation I believe.

class TicTacToeAi:
    """
    Class for simple tic-tac-toe AI algorithm. It chooses the next step 
    according to the following algorithm:
    1) Testing every empty field if it can win the game by placing its symbol there
    2) Testing every empty field the opponent can win the game, it tries to block it
    3) Otherwise chooses randomly
    """

    def __init__(self, aiSymbol, userSymbol):
        """
        Constructor
        Args:
            aiSymbol: tic-tac-toe symbol of the AI
            userSymbol: tic-tac-toe symbol of the opponent
        """
        self.aiSymbol = aiSymbol
        self.userSymbol = userSymbol

    def isGameWon(self, gameField):
        """
        Args:
            gameField: NxN array where empty fields are "n_i", the rest can be custom
        Returns:
            True or False if the game is won
        """
        def checkArray(array):
            c = array[0]
            if c == n_i:
                return False
            match = True
            for i in range(1, len(array)):
                if c != array[i]:
                    match = False
                    break
            return match

        #check horizontally
        for i in range(gameField.shape[0]):
            if checkArray(gameField[i]):
                return True

        #check vertical lines
        for i in range(gameField.shape[1]):
            if checkArray(gameField[:,i]):
                return True

        #check down diagonal
        c = gameField[0][0]
        if c != n_i:
            match = True
            for i in range(1, gameField.shape[0]):
                if c != gameField[i][i]:
                    match = False
                    break
            if match:
                return True

        #check up diagonal
        c = gameField[-1][0]
        if c != n_i:
            match = True
            for i in range(1, gameField.shape[0]):
                if c != gameField[-i - 1][i]:
                    match = False
                    break
            if match:
                return True
        
        return False

    def aiNextStep(self, gameField):
        """
        Makes the next step according to its internal logic.
        Args:
            gameField: NxN array where fields can have the following values:
                            - aiSymbol
                            - userSymbol
                            - n_i
        Returns:
            i, j: Indices of the next step of AI. Range: 0...N-1
        """
        if n_i in gameField:
            for i in range(gameField.shape[0]):
                for j in range(gameField.shape[1]):
                    if gameField[i][j] == n_i:
                        #robot wins in next round                      
                        gameField[i][j] = self.aiSymbol
                        if self.isGameWon(gameField):
                            return i, j
                        gameField[i][j] = n_i

            for i in range(gameField.shape[0]):
                for j in range(gameField.shape[1]):
                    if gameField[i][j] == n_i:
                        #prevent player winning in next round
                        gameField[i][j] = self.userSymbol
                        if self.isGameWon(gameField):
                            return i, j
                        gameField[i][j] = n_i   
            
            while True:
                i  = random.randint(0, gameField.shape[0] - 1)
                j  = random.randint(0, gameField.shape[1] - 1)
                if gameField[i][j] == n_i:
                    return i, j