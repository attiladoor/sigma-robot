class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Logger:
    """
    Helper function to provoide colorful log messages
    """
    def __init__(self, enabled, prefix=None):
        self.enableDebug = enabled
        self.prefix = prefix

    def system(self, message):
        if self.prefix:
            print(bcolors.OKGREEN + "[SYSTEM] " + self.prefix + " - " + message + bcolors.ENDC)
        else:
            print(bcolors.OKGREEN + "[SYSTEM] " + message + bcolors.ENDC)

    def fail(self, message):
        if self.prefix:
            print(bcolors.FAIL + "[FAIL] " + self.prefix + " - " + message + bcolors.ENDC)
        else:
            print(bcolors.FAIL + "[FAIL] " + message + bcolors.ENDC)
        
    def debug(self, message):
        if self.enableDebug:
            if self.prefix:
                print(bcolors.OKBLUE + "[DEBUG] " + self.prefix + " - " + message + bcolors.ENDC)
            else:
                print(bcolors.OKBLUE + "[DEBUG] " + message + bcolors.ENDC)

    def warning(self, message):
        if self.enableDebug:
            if self.prefix:
                print(bcolors.WARNING + "[WARNING] " + self.prefix + " - " + message + bcolors.ENDC)
            else:
                print(bcolors.WARNING + "[WARNING] " + message + bcolors.ENDC)

