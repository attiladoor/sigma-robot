from tictactoe import TicTacToe

# 2022-06-14: Fairly sure this class is unused in the robot-project, it is part of legacy code in the old logical-opponent implementation I believe.

def playerOne(game):
	stateIsSet = False
	while stateIsSet == False:
		print("Player one enter: ")
		x, y = [int(x) for x in input().split()]
		print("Entered: " + str(x) + " and: " + str(y))
		stateIsSet = game.setState(x, y, "X")
		if game.isGameWon():
			print("Player one wins!")
			game.stop()

def playerTwo(game):
	stateIsSet = False
	while stateIsSet == False:
		print("Player two enter: ")
		x, y = [int(x) for x in input().split()]
		print("Entered: " + str(x) + " and: " + str(y))
		stateIsSet = game.setState(x, y, "O")
		if game.isGameWon():
			print("Player two wins!")
			game.stop()
	
def draw(game):
	print(str(self.getGameBoard()))
	
x = TicTacToe(playerOne, playerTwo, resetGame, draw)

x.run()

print("Program endded.")
