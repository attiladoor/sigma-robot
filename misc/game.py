from datetime import datetime

# Use this as base for game.
# Inherit from it, and override update/draw

# 2022-06-14: Fairly sure this class is unused in the robot-project, it is part of legacy code in the old logical-opponent implementation I believe.
class Game:
	running = False
	def __init__(self):
		self.running = False
	
	# This can be called multiple times!
	def run(self):
		if self.running:
			return
			
		self.running = True
		startTime = datetime.now()
		endTime = datetime.now()
		deltaMilliseconds = 0.0
		
		while self.running:
			startTime = datetime.now()
			
			self.update(deltaMilliseconds)	
			self.draw()
			
			# Nothing fancy, just delta time calculation.
			endTime = datetime.now()
			deltaTime = endTime - startTime
			deltaMilliseconds = deltaTime.total_seconds()
	
	def update(self, deltaTime):
		pass
		
	def draw(self):
		pass
		
	def isGameWon(self):
		pass
		
	def isGameWon(self, gameField):
		pass
		
	def stop(self):
		self.running = False