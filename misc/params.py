import numpy as np

##### Various parameters used in the robot project gathered in one file ####

###### Image processor params ######
#x and y array indexes
x_idx = 2
y_idx = 3

# Filtering and merging params
theta_e = 0.05
theta_filter = 5 * theta_e
distance_e = 0.2 #20% of image
distance_merg = 30
# Resolutions and thresholds for Hugh Transform
hough_rho_res = 0.1
hough_theta_res = np.pi / 360
hough_vote_threshold = 60 # Num of votes to consider a line valid
# Canny edge detection parameters
canny_low = 50 # Intensity
canny_high = 150
canny_apt_size = 3 # Pixels

# Symbol threshold in order to consider valid classification
symbol_threshold = 0.8
# Number of samples a classified symbol needs to be stable to be considered valid
stable_samples = 3
# Limits to extract white pixels from the image to reduce noise
lower_white = np.array([70, 70, 70], dtype = "uint8")
upper_white = np.array([255, 255, 255], dtype = "uint8")


###### Image handler params ######
CAMERA_W = 1280 # Width of raw camera image
CAMERA_H = 720 # Height of raw camera image


###### Classifier params ######
adaptive_size = 3 # Adaptive tresholding mask size (pixels)
adaptive_constant = 0 # Adaptive tresholding bias


###### Grid plot and image-utils params ######
# indexes
rho_plot_i = 0
theta_plot_i = 1
x_plot_i = 0
y_plot_i = 1
grid_lines_num = 4
grid_field_frame_px = 5


###### Robot (main) params ######
X_SIZE = 150
Y_SIZE = 150
X0 = 100
Y0 = -75
Z_UP = 30
Z_DOWN = 20
XREAD = 75
YREAD = 0
ZREAD = 140
SPEED = 20000

N_i = '_'
X_i = 'x'
O_i = 'o'


###### Plotter params ######
# Maximum x and y limits for grid (inches)
maxX_ = 3
maxY_ = 3
# Draw-offset from corner
draw_X_offset_x = 0.1
draw_X_offset_y = 0.1
O_radius_ = 0.8 # Radius (inches) of O-ring
O_resolution_ = 100 # Angle-resolution of points