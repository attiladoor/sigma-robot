import sys
import os
sys.path.append(os.path.join(os.path.dirname(__file__), './image'))
sys.path.append(os.path.join(os.path.dirname(__file__), './objects'))
sys.path.append(os.path.join(os.path.dirname(__file__), './misc'))
sys.path.append(os.path.join(os.path.dirname(__file__), './uArm-Python-SDK'))
sys.path.append(os.path.join(os.path.dirname(__file__), './xy_plotter'))
import argparse
import cv2

import numpy as np 
import time
import threading

from image_processor import ImageProcessor

filename = 'output2.avi'
saved_name = 'savedFrame.png'
saved_name_warped = 'savedFrame_warped.png'
cap = cv2.VideoCapture(filename)

corner_points_1 = ((115, 104), (480, 104), (560, 364), (37, 368))
corner_points_2 = ((125, 180), (480, 180), (635, 315), (0, 330))

points = []
def click_point(event, x, y, flags, param):
    global points
    if event == cv2.EVENT_LBUTTONDOWN:
        points.append((x, y))
        
def calibrate_points(cap):
  print("Click to insert the 4 corners of the grid in the image, then press q.")
  while(cap.isOpened()):
    ret, frame = cap.read()   
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    for point in points:
        cv2.circle(gray, point, 3, (255, 0, 0), -1)
    cv2.namedWindow("calib_image")
    cv2.setMouseCallback("calib_image", click_point)
    cv2.imshow("calib_image", gray)
    time.sleep(0.1)  
    if cv2.waitKey(1) & 0xFF == ord('q'):
        cv2.destroyAllWindows()
        break 

calibrate_points(cap)
if(len(points) != 4):
  raise ValueError()

while(cap.isOpened()):
    ret, frame = cap.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imwrite(saved_name, gray) 

    cv2.imshow('frame', gray)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    
    rect = np.float32(points)
    (tl, tr, br, bl) = rect

    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    dst = np.array([
		[0, 0],
		[maxWidth - 1, 0],
		[maxWidth - 1, maxHeight - 1],
		[0, maxHeight - 1]], dtype = "float32")

    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(frame, M, (maxWidth, maxHeight))
    
    #cv2.imshow('warped', warped)
    cv2.imwrite(saved_name_warped, warped)
    
    gray_warped = cv2.cvtColor(warped, cv2.COLOR_BGR2GRAY)
    #gray_warped = cv2.adaptiveThreshold(gray_warped, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 2)
    edges = cv2.Canny(gray_warped,50,150,apertureSize = 3)
    
    cv2.imshow('canny',edges)

    cv2.imshow('houghlines',gray_warped)
    lines = cv2.HoughLines(edges,0.1,np.pi/360, 30)
    for line in lines:
        rho = line[0][0]
        theta = line[0][1]
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))

        cv2.line(gray_warped,(x1,y1),(x2,y2),(0,0,255),2)
        cv2.imshow('houghlines',gray_warped)
        cv2.waitKey(1)
    
    imageProcessor = ImageProcessor(maxWidth, maxHeight, ifPlot=True, isDebug=True)
    try:
      upd_image, board = imageProcessor.updateImage(warped)
      cv2.imshow('Updated', upd_image)
      cv2.waitKey(1)
    except Exception as e:
      print(e)

    time.sleep(0.1)


# Corner detection

cap.release()
cv2.destroyAllWindows()
