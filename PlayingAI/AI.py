import sys
import os
import numpy as np
import os
import random
DirPath = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '../misc'))
from params import *

class AI:

    def __init__(self, board, discountFactor, eps, learningRate, aiSymbol):
        """
        Constructor
        Args:
            board: board-object of the game
            discountFactor: Reward propergation discount
            eps: Epsilon value for epsilon-greedy discovery
            learningRate: Factor for how quickly the AI takes on new q-table values when updating
            aiSymbol: tic-tac-toe symbol of the AI
        """
        self.qFilePath = DirPath + '\QTable_' + aiSymbol + '.txt'
        self.board = board
        self.nStates = len(board.layoutToState)
        self.symbol = aiSymbol
        # Initialize optimistic Q-values (reward of 1)
        self.qTable = 1 * np.ones((self.nStates, 9))
        self.startingStates = []
        self.performedActions = []
        self.readFromFile()
        self.eps = eps
        self.initEps = eps
        self.discountFactor = discountFactor
        self.learningRate = learningRate
        
    def readFromFile(self):
        """
        Read Q-table from file
        specified by symbol if it exists
        """
        if not os.path.isfile(self.qFilePath):
            # If file does not exist, create
            print('The file' + self.qFilePath + ' does not exist, initiating file.')
            self.saveFile()
            return
        # Otherwise, overwrite initial values from file
        self.qTable = np.loadtxt(self.qFilePath, dtype = float, delimiter = ',')

    def saveFile(self):
        """
        Saves Q-values as text file to be read
        """
        np.savetxt(self.qFilePath, self.qTable, delimiter = ',')

    def performAction(self, randomAction = False):
        """
        Performs action based on current state and Q-table
        Args:
            k: Time index of the game (not currently used to adapt selection)
            randomAction: Flag to completely randomize the selection
        Returns:
              Index of tile to put as occupied
        """
        state = self.board.getCurrentStateIndex()
        # Get actions that are valid in current state
        availableActions = self.board.getAvailableSlots()
        # If random action, select randomly and skip q-table calculations
        if randomAction or random.random() <= self.eps:
            sel_action = random.choice(availableActions)
        else:
            # Get maximum Q-value from available actions by look-up in Q-table
            qRow = self.qTable[state, availableActions]
            action = np.argmax(qRow)
            sel_action = availableActions[action]
        
        # Perform action
        self.board.setTile(sel_action, self.symbol)
        
        row = sel_action // 3
        col = sel_action - row * 3
        # Return row and col of action
        return state, sel_action, row, col

    def updateQTable(self, reward):
        for i in range(len(self.startingStates) - 1, -1, -1):
            # For each visited state and action, calculate discounted reward and update q-table and state counter
            state = self.startingStates[i]
            action = self.performedActions[i]
            # If at the last state (== winning state) no next state is available, value is reward itself
            if i == len(self.startingStates) - 1:
                self.qTable[state, action] = reward
            # Otherwise, perform regular update equation:
            else:
                self.qTable[state, action] = self.qTable[state, action] \
                + self.learningRate * (self.discountFactor * nextmax \
                - self.qTable[state, action])
            # Special case: According to theory we should have max here, but since next state belongs to opponent, the AI have no control of the value in the next state.
            # Thus taking the mean of the values of the possible opponent-actions in the next state, we will reach a much better representation of the value of that state from the AI's point of view
            nextmax = np.mean(self.qTable[state, :])
        # After finished update, empty replay-lists
        self.startingStates = []
        self.performedActions = []

    def updateEps(self):
        # Update new epsilon to be 95% of previous value (decay)
        self.eps = self.eps * 0.95

    def resetValues(self):
        # Will reset the q-table to all ones and reset epsilon
        self.qTable.fill(1)
        self.eps = self.initEps

    def addStateAction(self, state, action):
        # Will add state and action to object's member lists, for replay-learning after a game is finished (technically you could update after each move, more of a matter of taste)
        self.startingStates.append(state)
        self.performedActions.append(action)
