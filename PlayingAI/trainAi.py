from AI import AI
from Board import Board
import matplotlib.pyplot as plt

# Script to automatically and quickly learn the Q-table based on a random opponent. 
# With enough iterations (a couple hundred thousands are usually enough) it should get enough state-coverage of all possible playthroughs and learn the optimal playstyle.
# Run the playAI-script after this will usually get a quite good feel for whether or not the AI is trained enough, but a plot will also be given at the end of the game that can be investigated

symbols = ['_', 'x', 'o']
board = Board(symbols)
# In this script, only train 'x' AI vs random opponent. Change indexes to learn the 'o' table, you would also need to change the order of performed actions, but usually the randomly learned q-table is actually good enough to play versus.
# Otherwise, funny enough, it was discovered that the 'o' table is basically the inverse of the x-table and vice-versa, which makes sense after some thinking
Ai = AI(board, 0.9, 0, 0.4, symbols[1])
randomAi = AI(board, 0.95, 0.3, 1, symbols[2])

# Initiate win/lose/draw counters for plotting
x_wins = []
o_wins = []
draws = []
counter = []

episodes = 1000
battles_per_episode = 1000
for i in range(1, episodes + 1):
    # Initiate sub-counters for the wins and draws
    x_wins_i = 0
    o_wins_i = 0
    draws_i = 0
    for j in range(battles_per_episode):
        while True:
            ret = Ai.performAction()
            Ai.addStateAction(ret[0], ret[1])
            randomAi.addStateAction(ret[0], ret[1])
            isend = board.isWinner(symbols[1]) or board.isDraw()
            if isend:
                break
            ret = randomAi.performAction(True)
            Ai.addStateAction(ret[0], ret[1])
            randomAi.addStateAction(ret[0], ret[1])
            isend = board.isWinner(symbols[2]) or board.isDraw()
            if isend:
                break

        # Calulate reward
        reward_x = -0.5
        reward_o = -0.5
        if board.isDraw():
            draws_i = draws_i + 1
        if board.isWinner(symbols[1]):
            x_wins_i = x_wins_i + 1
            reward_x = 1
            reward_o = -1
        if board.isWinner(symbols[2]):
            o_wins_i = o_wins_i + 1
            reward_x = -1
            reward_o = 1

        Ai.updateQTable(reward_x)
        randomAi.updateQTable(reward_o)
        board.resetBoard() 
    # Only save q-table after each episode
    print(str(i * battles_per_episode) + ' iterations performed')
    Ai.updateEps()
    Ai.saveFile()
    randomAi.saveFile()

    # Add number of wins and draws as percentages to vectors, to be used by the results-plot
    x_wins.append(x_wins_i * 100 / battles_per_episode)
    o_wins.append(o_wins_i * 100 / battles_per_episode)
    draws.append(draws_i * 100 / battles_per_episode)
    counter.append((i - 1) * battles_per_episode)
    x_wins.append(x_wins_i * 100 / battles_per_episode)
    o_wins.append(o_wins_i * 100 / battles_per_episode)
    draws.append(draws_i * 100 / battles_per_episode)
    counter.append(i * battles_per_episode)
Ai.saveFile()
randomAi.saveFile()

# Plot results for inspection
plt.ylabel('Game outcomes in %')
plt.xlabel('Game number')

plt.plot(counter, draws, 'r-', label='Draw')
plt.plot(counter, x_wins, 'g-', label='Player X wins')
plt.plot(counter, o_wins, 'b-', label='Player O wins')
plt.legend(loc='best', shadow=True, fancybox=True, framealpha =0.7)
plt.show()