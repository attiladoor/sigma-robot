from AI import AI
from Board import Board

# Test-script for manually playing against the AI, without having to run the whole robot.py script (more for quick-tests when debugging)

symbols = ['_', 'x', 'o']
board = Board(symbols)
AiChoice = 0  # 0 indicating AI starting as 'x'
AiSymbol = symbols[AiChoice + 1]
playerSymbol = symbols[2 - AiChoice]
Ai = AI(board, 0.8, 0, 0.4, AiSymbol)

while True: # Outer loop for multiple games
    nextturn = AiChoice
    while True:
        if nextturn == 0: # AI's turn
            ret = Ai.performAction()
            # Since playing vs human should give better experience than the automatic random-opponent training script, take the opportunity to save the action-state pairs and update the Q-table
            Ai.addStateAction(ret[0], ret[1]) 
            board.writeBoard()
            nextturn = 1
            # Check if game ended
            isend = board.isWinner(AiSymbol) or board.isDraw()
            if isend:
                break
        if nextturn == 1: # Player turn   
            while True: # Until valid move is selected
                state = board.getCurrentStateIndex()
                action = int(input('Insert your action (1-9): ')) - 1
                validActions = board.getAvailableSlots()
                if action in validActions:
                    board.setTile(action, playerSymbol)
                    break
                else:
                    print('Invalid choice, retry.')
            Ai.addStateAction(state, action)          
            board.writeBoard()
            nextturn = 0
            # Check if game ended
            isend = board.isWinner(playerSymbol) or board.isDraw()
            if isend:
                break

    # Calulate reward
    reward = 0
    if board.isWinner(AiSymbol):
        print('AI won!')
        reward = 1
    if board.isWinner(playerSymbol):
        print('Player won!')
        reward = -1       
    if board.isDraw():
        print('Draw!')    

    # Update Q-table from the game-outcome, save and reset the board for next game
    Ai.updateQTable(reward)
    Ai.saveFile()
    board.resetBoard()