import sys
import os
import numpy as np
sys.path.append(os.path.join(os.path.dirname(__file__), '../misc'))
from params import *

class Board:

    def __init__(self, symbols):
        """
         Constructor for the Board-class, also creates the state-dictionary
         """
        self.symbols = symbols
        self.tiles = np.array([char for char in self.symbols[0] * 9])
        self.layoutToState = {}

        # Create state-dictionary from scratch
        symbolCounters = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        stateKey = ''.join(self.tiles)
        # First state in dictionary is empty field with index 0
        self.layoutToState[stateKey] = 0
        stateCounter = 1
        # Loop through all possible combinations of states and assign a counter-value to it
        while ''.join(self.tiles) != self.symbols[2] * 9:
            # Create new state, trinary counting (0->1->2->0->1...)
            symbolCounters[0] = symbolCounters[0] + 1
            if symbolCounters[0] == 3:
                symbolCounters[0] = 0
            self.tiles[0] = self.symbols[symbolCounters[0]]
            # Loop through all remaining counters, first is special case
            for k in range(1, len(symbolCounters)):
                # If previous "bit" value is something other than 0, next positions will remain unchanged
                if symbolCounters[k - 1] != 0:
                    break
                symbolCounters[k] = symbolCounters[k] + 1
                if symbolCounters[k] == 3:
                    symbolCounters[k] = 0
                self.tiles[k] = self.symbols[symbolCounters[k]]    
            # Add incremented state to dictionary
            stateKey = ''.join(self.tiles)
            # First check if state is invalid, i.e. if the number of
            # Xs and Os differs by more than 1, then this state should never be reached
            countX = stateKey.count(self.symbols[1])
            countO = stateKey.count(self.symbols[2])
            if abs(countX - countO) > 1:
                continue
            # If not larger than 1, state is valid and added to dictionary
            self.layoutToState[stateKey] = stateCounter
            stateCounter = stateCounter + 1
        # After state-dictionary is created, reset the tiles
        self.resetBoard()


    def resetBoard(self):
        """
        Resets the board
        """
        for i in range(len(self.tiles)):
            self.tiles[i] = self.symbols[0]


    def setTile(self, position, symbol):
        """
        Set a tile in the board
        Args:
            position: 0-8, position on the board
            symbol: which symbol to set in the tile
        Returns:
              Boolean True/False if the operation was a success
        """
        # If position is not empty, return operation as false
        if self.tiles[position] != self.symbols[0]:
            return False
        self.tiles[position] = symbol
        return True


    def clearTile(self, pos):
        """
        Reset the tile specified by 'pos' to an empty state
        """
        self.tiles[pos] = self.symbols[0]

    def updateState(self, state):
        """"
        Updates the whole board based on external state-get
        Args:
            state: Current state as string/array of chars
        """
        self.tiles = state


    def getCurrentStateIndex(self):
        """"
        Get the current state as an int-value
        Returns:
            int-value of the current state
        """
        return self.layoutToState[''.join(self.tiles)]

    def getAvailableSlots(self):
        """"
        Get the non-occupied tiles
        Returns:
            int-array of non-occupied indixes
        """
        freeSlots = []
        for i, tile in enumerate(self.tiles):
            if tile == self.symbols[0]:
                freeSlots.append(i)
        return freeSlots

    def isWinner(self, symbol):
        """
        Checks if the symbol is a winner in the current state
        Args:
            symbol: symbol to check
        Returns:
              True/False if winner
        """
        #Sideways
        tilesRows = [''.join(self.tiles[0:3]), ''.join(self.tiles[3:6]), ''.join(self.tiles[6:9])]
        if symbol + symbol + symbol in tilesRows:
            return True
        #Up/Down (two empty tiles in between)
        for k in range(3):
            if self.tiles[k] + self.tiles[k+3] + self.tiles[k+6] == symbol + symbol + symbol:
                return True
        #Diagonal
        if self.tiles[0] + self.tiles[4] + self.tiles[8] == symbol + symbol + symbol or self.tiles[2] + self.tiles[4] + self.tiles[6] == symbol + symbol + symbol:
            return True

    def isDraw(self):
        """""
        Checks if the game is in a draw-state
        Returns: 
            True/False if draw
        """""
        return self.symbols[0] not in ''.join(self.tiles) and not self.isWinner(self.symbols[1]) and not self.isWinner(self.symbols[2])

    def writeBoard(self):
        print('------------------')
        print('|' + self.tiles[0] + '|' + self.tiles[1] + '|' + self.tiles[2] + '|')
        print('------------------')
        print('|' + self.tiles[3] + '|' + self.tiles[4] + '|' + self.tiles[5] + '|')
        print('------------------')
        print('|' + self.tiles[6] + '|' + self.tiles[7] + '|' + self.tiles[8] + '|')
        print('------------------')

    def to1DGameState(self, gameState):
        """
        Returns a 1D-vector of the state from 2D gameState
        """
        return str(np.reshape(gameState, (1, 9)), 'utf-8')

    def to2DGameState(self):
        """
        Returns a 2D-vector of the state from the internal 1D state
        """
        return np.chararray(np.reshape(self.tiles, (3, 3)))