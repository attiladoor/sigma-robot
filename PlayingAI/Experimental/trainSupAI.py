from AI import AI
from SupAI import SupAI
from Board import Board
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
from collections import deque

episodes = 1000
battles_per_episode = 100
discount_factor = 0.9
eps = 1
learning_rate = 0.01

symbols = ['_', 'x', 'o']
board = Board(symbols)
# In this script, only train 'x' AI vs random opponent
randomAi = SupAI(board, discount_factor, eps, learning_rate, symbols[2])
supAi = SupAI(board, discount_factor, eps, learning_rate, symbols[1])

# Initiate win/lose/draw counters for plotting
x_wins = []
o_wins = []
draws = []
counter = []
streaks = 0

for i in range(1, episodes + 1):
    # Initiate sub-counters for the wins and draws
    x_wins_i = 0
    o_wins_i = 0
    draws_i = 0
    invalid_moves_i = 0

    # State & Value-arrays for batch learning
    states = []
    values = []
    for j in range(battles_per_episode):
        done = False
        tmp_values = []

        while True:
            # First, perform action from current state
            ret = supAi.performAction()
            action = ret[0]
            # Next, get the state where we ended up and predict value of this state
            state = supAi.getFeatureVector()
            value = supAi.evaluator.predict(state)[0][0]
            states.append(state)
            tmp_values.append(value)
            # If winning or draw condition achieved, end game
            isend = board.isWinner(symbols[1]) or board.isDraw()
            if isend:
                break

            # Perform random opponent action, but supAi should also evaluate these states
            # Should we tho? Maybe not, we needed in the Q-table approach but probably not in NN
            ret = randomAi.performAction(randomAction=True)
            action = ret[0]
            # Calculate value of where we ended up and save
            #state = supAi.getFeatureVector()
            #value = supAi.evaluator.predict(state)
            #states.append(state)
            #values.append(value[0])
            # Check win/draw condition
            isend = board.isWinner(symbols[2]) or board.isDraw()
            if isend:
                break

        # Calulate reward
        ai_reward = 0
        if board.isDraw():
            draws_i = draws_i + 1
        if board.isWinner(symbols[1]):
            x_wins_i = x_wins_i + 1
            ai_reward = 1
        if board.isWinner(symbols[2]):
            o_wins_i = o_wins_i + 1
            ai_reward = -1
        
        # Replace last value with reward and shift other values
        tmp_values.pop(0) # Completely remove first value from list
        tmp_values.append(ai_reward)

        # Test: Put set all values to reward instead of simply appending reward to the end
        tmp_values = np.array(tmp_values)
        #tmp_values[:] = ai_reward
        # Append battle-values to end of total values-struct
        values = np.concatenate((values, tmp_values))
        # Reset board for next game
        board.resetBoard() 

    # After each episode, batch-train
    states = np.array(states)
    # Shuffle arrays before training
    shuffler = np.random.permutation(len(values))
    values = values[shuffler]
    states = states[shuffler]
    states = states.reshape(-1, 18) 

    # Train model (fit) using resulting states and values from the game
    supAi.trainModel(states, values)
   
    
    # Only save q-table after each episode
    print(str(i * battles_per_episode) + ' battles performed (' + str(i) + ' episodes)')
    supAi.saveModel()
    # Also decay epsilon after each episode, to get converging behaviour
    supAi.decayEps()

    # Add number of wins and draws as percentages to vectors
    x_wins.append(x_wins_i * 100 / battles_per_episode)
    o_wins.append(o_wins_i * 100 / battles_per_episode)
    draws.append(draws_i * 100 / battles_per_episode)
    counter.append((i - 1) * battles_per_episode)
    # Append a second time to get 'flat' plots
    x_wins.append(x_wins_i * 100 / battles_per_episode)
    o_wins.append(o_wins_i * 100 / battles_per_episode)
    draws.append(draws_i * 100 / battles_per_episode)
    counter.append(i * battles_per_episode)
    if(x_wins[-1] == battles_per_episode):
        streaks += 1
        if streaks == 100:
            break
    else:
        streaks = 0
supAi.saveModel()

# Plot results for inspection
plt.ylabel('Game outcomes in %')
plt.xlabel('Game number')

plt.plot(counter, draws, 'k-', label='Draw')
plt.plot(counter, x_wins, 'g-', label='Player X wins')
plt.plot(counter, o_wins, 'b-', label='Player O wins')
plt.legend(loc='best', shadow=True, fancybox=True, framealpha =0.7)
plt.show(block=True)