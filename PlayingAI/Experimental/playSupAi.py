from AI import AI
from SupAI import SupAI
from Board import Board

symbols = ['_', 'x', 'o']
epsilon = 0
discount_factor = 0.95
learning_rate = 0.1
board = Board(symbols)
AiChoice = 0  # 0 indicating AI starting as 'x'
AiSymbol = symbols[AiChoice + 1]
playerSymbol = symbols[2 - AiChoice]
Ai = SupAI(board, discount_factor, epsilon, learning_rate, AiSymbol)
batch_size = 10

while True: # Outer loop for multiple games
    nextturn = AiChoice
    while True:
        if nextturn == 0:
            ret = Ai.performAction()
            action = ret[0]
            board.writeBoard()
            nextturn = 1
            isend = board.isWinner(AiSymbol) or board.isDraw()
            if isend:
                break

        if nextturn == 1:   
            while True:
                action = int(input('Insert your action (1-9): ')) - 1
                validActions = board.getAvailableSlots()
                if action in validActions:
                    board.setTile(action, playerSymbol)
                    break
                else:
                    print('Invalid choice, retry.')   
            board.writeBoard()
            nextturn = 0
            isend = board.isWinner(playerSymbol) or board.isDraw()
            if isend:
                break

    # Calulate reward
    reward = 0
    if board.isWinner(AiSymbol):
        print('AI won!')
        reward = 1
    if board.isWinner(playerSymbol):
        print('Player won!')
        reward = -1       
    if board.isDraw():
        print('Draw!')
    board.resetBoard()