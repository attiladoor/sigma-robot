from cgitb import reset
from cmath import inf
import sys
import os
import numpy as np
import random
from collections import deque

DirPath = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(os.path.dirname(__file__), '../../misc')) # Assuming we are in the 'Experimental' folder still
from params import *
from keras.layers.core import Dense, Dropout
from keras.models import Sequential
from keras.optimizers import SGD, Adam
from keras import initializers

# New approach, consider problem not as a RL-unsurpervised scenario, 
# rather gather data from play-through and 'label' them to perform regular supervised learning
# https://www.kaggle.com/dhanushkishore/a-self-learning-tic-tac-toe-program
class SupAI:

    def __init__(self, board, discountFactor, eps, learningRate, aiSymbol):
        """
        Constructor
        Args:
            board: board-object of the game
            discountFactor: Reward proporgation discount
            eps: Epsilon value for epsilon-greedy discovery
            learningRate: Factor for how quickly the AI takes on new q-table values when updating
            aiSymbol: tic-tac-toe symbol of the AI
        """
        self.model_path = DirPath + '\\saved_evaluator_model_' + aiSymbol + '.hdf5'
        self.board = board
        self.symbol = aiSymbol
        self.epsilon = eps
        self.init_eps = eps
        self.discount_factor = discountFactor
        self.epsilon_decay = 0.95
        self.epsilon_min = 0.01
        self.learning_rate = learningRate
        self.evaluator = self.build_evaluator()
        self.readFromFile()
        
    def build_evaluator(self):
        # Neural Net for Deep-Q learning Model
        model = Sequential()
        model = Sequential()
        model.add(Dense(18, input_dim=18, kernel_initializer='he_normal', activation='relu'))
        model.add(Dense(9, kernel_initializer='he_normal', activation='relu'))
        model.add(Dense(1, kernel_initializer='he_normal', activation='relu'))
        model.add(Dense(1, kernel_initializer='he_normal'))
       
        model.compile(loss="mean_squared_error",
                        optimizer=Adam(lr=self.learning_rate))
        return model
    
    def readFromFile(self):
        """
        Read Q-table from file
        specified by symbol if it exists
        """
        if not os.path.isfile(self.model_path):
            # If file does not exist, create
            print('The file' + self.model_path + ' does not exist, initiating file.')
            self.saveModel()
            return
        else:
            # Otherwise, load model weights from file
            self.evaluator.load_weights(self.model_path)

    def saveModel(self):
        """
        Saves NN-model as hd5f format
        """
        self.evaluator.save(self.model_path)

    def getFeatureVector(self):
        feature_vector = np.zeros(18)
        for i in range(9):
            if self.board.tiles[i] == self.board.symbols[1]:
                feature_vector[2*i] = 1
            elif self.board.tiles[i] == self.board.symbols[2]:
                feature_vector[2*i + 1] = 1
        return feature_vector.reshape((1, 18))

    def performAction(self, randomAction = False):
        """
        Performs action based on current state and Q-table
        Args:
            k: Time index of the game (not currently used to adapt selection)
            randomAction: Flag to completely randomize the selection
        Returns:
              Index of tile to put as occupied
        """

        # Get actions that are valid in current state
        available_actions = self.board.getAvailableSlots()
        # If random action, select randomly and skip q-table calculations
        if randomAction or random.random() <= self.epsilon:
            sel_action = random.choice(available_actions)
        else:
            qRow = self.getQValues(available_actions)
            action = np.argmax(qRow)
            sel_action = available_actions[action]
        # Perform action
        self.board.setTile(sel_action, self.symbol)
        
        row = sel_action // 3
        col = sel_action - row * 3
        # Return row and col of action
        return sel_action, row, col

    def getQValues(self, actions):
        qValues = np.zeros(len(actions))
        for idx, possible_action in enumerate(actions):
            # For each possible move, get feature vector of that state and evaluate value using model
            self.board.setTile(possible_action, self.symbol)
            new_feat_vec = self.getFeatureVector()
            state_q = self.evaluator.predict(new_feat_vec)[0][0]
            # Reset move
            self.board.clearTile(possible_action)
            qValues[idx] = state_q
        return qValues


    def trainModel(self, states, corrected_scores):
        self.evaluator.fit(states, corrected_scores, batch_size=64, epochs=10, verbose=1)

    def decayEps(self):
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def resetValues(self):
        # Add model-reset here later
        self.epsilon = self.init_eps
