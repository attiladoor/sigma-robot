import sys
import os
from matplotlib.pyplot import axis
import matplotlib.pyplot as plt
import numpy as np

from keras.layers.core import Dense
from keras.layers import InputLayer
from keras.models import Sequential
from keras.optimizers import SGD, Adam

# Test class to debug tensorflow-issue with near constant outputs
class TestAISimple:

    def __init__(self, learningRate):
        self.learning_rate = learningRate
        self.evaluator = self.build_evaluator()
        self.evaluator.summary()
        
    def build_evaluator(self):
        # Simple network, should be easier to debug
        model = Sequential()
        model.add(Dense(3, input_dim=3, kernel_initializer='he_normal', activation='relu'))
        model.add(Dense(18, kernel_initializer='he_normal', activation='relu'))
        model.add(Dense(1, kernel_initializer='he_normal', activation='relu'))
        model.add(Dense(1, kernel_initializer='he_normal'))
       
        model.compile(loss="mean_squared_error",
                        optimizer=Adam(lr=self.learning_rate))
        return model


    def trainModel(self, states, corrected_scores):
        self.evaluator.fit(states, corrected_scores, batch_size=64, epochs=100, verbose=1)


def get_single_feat_vec(x):
    return feat_vec_arr[0]

def get_single_meas(x):
    return x[0]^3

def meas_func(x):
    meas_arr = x[:, 0] + np.power(x[:, 1], 2) - np.power(x[:, 2], 3)
    return meas_arr

### Main #####
if __name__ == "__main__":
    AI = TestAISimple(0.01)
    episodes = 10000
    feat_vec_arr = np.random.rand(episodes, 3) * 4 - 2
    corr_val_arr = meas_func(feat_vec_arr)

    AI.trainModel(feat_vec_arr, corr_val_arr)

    test_samples = 1000
    feat_vec_arr = np.random.rand(test_samples, 3) * 4 - 2
    corr_val_arr = feat_vec_arr[:, 0] + np.power(feat_vec_arr[:, 1], 2) - np.power(feat_vec_arr[:, 2], 3)

    results = AI.evaluator.evaluate(feat_vec_arr, corr_val_arr)
    print("test loss:", results)

    # Plot 3 different plots, where only one dimension is varying according to the x-axis (otherwise we would need a 4D plot, interesting)
    feat_vec_arr[:, 0] = np.linspace(-2, 2, test_samples)
    feat_vec_arr[:, 1].fill(np.random.uniform(-2, 2))
    feat_vec_arr[:, 2].fill(np.random.uniform(-2, 2))
    pred_val = AI.evaluator.predict(feat_vec_arr)
    y = meas_func(feat_vec_arr)
    plt.plot(feat_vec_arr[:, 0], y, color='r', label='True Values')
    plt.plot(feat_vec_arr[:, 0], pred_val, color='g', label='Pred Values')
    plt.show()

    feat_vec_arr[:, 1] = np.linspace(-2, 2, test_samples)
    feat_vec_arr[:, 0].fill(np.random.uniform(-2, 2))
    feat_vec_arr[:, 2].fill(np.random.uniform(-2, 2))
    pred_val = AI.evaluator.predict(feat_vec_arr)
    y = meas_func(feat_vec_arr)
    plt.plot(feat_vec_arr[:, 1], y, color='r', label='True Values')
    plt.plot(feat_vec_arr[:, 1], pred_val, color='g', label='Pred Values')
    plt.show()

    feat_vec_arr[:, 2] = np.linspace(-2, 2, test_samples)
    feat_vec_arr[:, 0].fill(np.random.uniform(-2, 2))
    feat_vec_arr[:, 1].fill(np.random.uniform(-2, 2))
    pred_val = AI.evaluator.predict(feat_vec_arr)
    y = meas_func(feat_vec_arr)
    plt.plot(feat_vec_arr[:, 2], y, color='r', label='True Values')
    plt.plot(feat_vec_arr[:, 2], pred_val, color='g', label='Pred Values')
    plt.show()

   

